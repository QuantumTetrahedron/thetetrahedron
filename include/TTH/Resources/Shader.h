
#ifndef THETETRAHEDRON_SHADER_H
#define THETETRAHEDRON_SHADER_H

#include <string>
#include <glm/glm.hpp>
#include <iostream>

namespace TTH {
    /**
     * \brief A wrapper for shaders.
     *
     * This class facilitates the use of shaders.
     * It allows an easy way of creating a shader (should be done by the ResourceManager anyway), and passing data via uniforms.
     */
    class Shader {
    public:
        Shader(const char* vertexFilePath, const char* fragmentFilePath);

        /**
         * Binds the shader as the currently active.
         */
        void Use() const;

        /**
         * Sets a bool uniform
         * @param name - The name of the variable inside the shader code.
         * @param value - The value to assign to that variable.
         */
        void SetBool(const std::string& name, bool value) const;

        /**
         * Sets an int uniform
         * @param name - The name of the variable inside the shader code.
         * @param value - The value to assign to that variable.
         */
        void SetInt(const std::string& name, int value) const;

        /**
         * Sets an float uniform
         * @param name - The name of the variable inside the shader code.
         * @param value - The value to assign to that variable.
         */
        void SetFloat(const std::string& name, float value) const;

        /**
         * Sets a vec2 uniform
         * @param name - The name of the variable inside the shader code.
         * @param v - A glm::vec2 with values to assign to that variable.
         */
        void SetVec2(const std::string& name, const glm::vec2& v) const;

        /**
         * Sets a vec2 uniform
         * @param name - The name of the variable inside the shader code.
         * @param x - The value to assign to the vector's x component
         * @param y - The value to assign to the vector's y component
         */
        void SetVec2(const std::string& name, float x, float y) const;

        /**
         * Sets a vec3 uniform
         * @param name - The name of the variable inside the shader code.
         * @param v - A glm::vec3 with values to assign to that variable.
         */
        void SetVec3(const std::string& name, const glm::vec3& v) const;

        /**
         * Sets a vec3 uniform
         * @param name - The name of the variable inside the shader code.
         * @param x - The value to assign to the vector's x component
         * @param y - The value to assign to the vector's y component
         * @param z - The value to assign to the vector's z component
         */
        void SetVec3(const std::string& name, float x, float y, float z) const;

        /**
         * Sets a vec4 uniform
         * @param name - The name of the variable inside the shader code.
         * @param v - A glm::vec4 with values to assign to that variable.
         */
        void SetVec4(const std::string& name, const glm::vec4& v) const;

        /**
         * Sets a vec4 uniform
         * @param name - The name of the variable inside the shader code.
         * @param x - The value to assign to the vector's x component
         * @param y - The value to assign to the vector's y component
         * @param z - The value to assign to the vector's z component
         * @param w - The value to assign to the vector's w component
         */
        void SetVec4(const std::string& name, float x, float y, float z, float w) const;

        /**
         * Sets a mat4 uniform
         * @param name - The name of the variable inside the shader code.
         * @param mat - A glm::mat4 with values to assign to that variable.
         */
        void SetMat4(const std::string& name, const glm::mat4& mat) const;

    private:
        void LoadFromFile(const char* vertexFilePath, const char* fragmentFilePath);
        void checkErrors(unsigned int shader, std::string type);

        friend class UniformBuffer;
        unsigned int id;
    };
}

#endif //THETETRAHEDRON_SHADER_H

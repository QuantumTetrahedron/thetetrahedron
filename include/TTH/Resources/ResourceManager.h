
#ifndef THETETRAHEDRON_RESOURCEMANAGER_H
#define THETETRAHEDRON_RESOURCEMANAGER_H

#include <map>
#include "Model.h"
#include "Texture.h"
#include "Font.h"
#include "UniformBuffer.h"

namespace TTH {

    class IniFile;

    /**
     * \brief A class for managing resources.
     *
     * The ResourceManager stores loaded assets like shaders, models, textures and fonts.
     *
     * The ResourceManager loads files specified in the game's configuration .ini file.
     * See the GameData/InitData.ini file for details.
     */
    class ResourceManager {
    public:
        /**
         * Gets a previously loaded shader.
         * Throws a std::out_of_range exception if the shader is not found.
         * @param name - The name of the shader to get.
         * @return A pointer to the requested shader.
         */
        static Shader* GetShader(const std::string& name);

        /**
         * Loads a shader from its files.
         * \todo Geometry and tesselation stages.
         * @param vertexShaderFilePath - A path to a file containing the code of the shader's vertex stage.
         * @param fragmentShaderFilePath  - A path to a file containing the code of the shader's fragment stage.
         * @param name - A name to give to the created shader.
         */
        static void LoadShader(const char* vertexShaderFilePath, const char* fragmentShaderFilePath,const std::string& name);

        /**
         * Removes a shader from the manager.
         * @param name - The name of the shader to remove.
         */
        static void DeleteShader(const std::string& name);

        /**
         * Gets a previously loaded 3D model.
         * Throws a std::out_of_range exception if the model is not found.
         * @param name - The name of the 3D model to get.
         * @return A pointer to the requested model.
         */
        static Model* GetModel(const std::string& name);

        /**
         * Loads a 3D model from its file.
         * @param filePath - A path to the file containing the model's data.
         * @param name - A name to give to the created model.
         */
        static void LoadModel(const char* filePath,const std::string& name);

        static bool HasModel(const std::string& name);
        /**
         * Removes a 3D model from the manager.
         * @param name - The name of the model to remove.
         */
        static void DeleteModel(const std::string& name);

        /**
         * Gets a previously loaded texture.
         * Throws a std::out_of_range exception if the texture is not found.
         * @param name - The name of the texture to get.
         * @return A pointer to the requested texture.
         */
        static Texture* GetTexture(const std::string& name);

        /**
         * Loads a texture from its file.
         * @param filePath - A path to a texture file.
         * @param name - A name to give to the created texture
         */
        static void LoadTexture(const char* filePath, const std::string& name);

        /**
         * Removes a texture from the manager.
         * @param name - The name of the texture to remove.
         */
        static void DeleteTexture(const std::string& name);

        /**
         * Gets a previously loaded font.
         * Throws a std::out_of_range exception if the font is not found.
         * @param name - The name of the font to get.
         * @return A pointer to the requested font.
         */
        static Font* GetFont(const std::string& name);

        /**
         * Loads a font from its file with a given height in pixels.
         * @param filePath - A path to a true type font file.
         * @param size - The size to load.
         * @param name - A name to give to the created font.
         */
        static void LoadFont(const char* filePath, unsigned int size, const std::string& name);

        /**
         * Removes a font from the manager.
         * @param name - The name of the font to remove.
         */
        static void DeleteFont(const std::string& name);

        /**
         * Gets a previously loaded shader designated for post process effects
         * @param name - The name of the effect
         * @return The requested effect's shader
         */
        static Shader* GetPostprocessEffect(const std::string& name);

        /**
         * Loads a shader designated for post process effects
         * @param vertexShaderPath - path to the vertex shader file
         * @param fragmentShaderPath - path to the fragment shader file
         * @param name - The name of the effect
         */
        static void LoadPostprocessEffect(const char* vertexShaderPath, const char* fragmentShaderPath, const std::string& name);

        /**
         * Removes a postprocess effect from the manager.
         * @param name - The name of the effect to remove.
         */
        static void DeletePostprocessEffect(const std::string& name);

        /**
         * Frees the memory.
         */
        static void CleanUp();

        static void CreateUniformBuffer(std::string name, unsigned int size);
        static UniformBuffer* GetUniformBuffer(std::string name);
        static void DeleteUniformBuffer(std::string name);
    private:
        friend class Game;
        static void LoadFromFile(IniFile& file);

        static std::map<std::string, Shader*> shaders;
        static std::map<std::string, Model*> models;
        static std::map<std::string, Texture*> textures;
        static std::map<std::string, Font*> fonts;
        static std::map<std::string, Shader*> postprocessEffects;
        static std::map<std::string, UniformBuffer*> uniformBuffers;

        template <typename T>
        static void FreeMap(std::map<std::string, T*>& map){
            for(auto p : map){
                delete p.second;
                p.second = nullptr;
            }
            map.clear();
        }
    };
}


#endif //THETETRAHEDRON_RESOURCEMANAGER_H

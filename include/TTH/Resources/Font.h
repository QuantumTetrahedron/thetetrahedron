
#ifndef THETETRAHEDRONTEST_FONT_H
#define THETETRAHEDRONTEST_FONT_H

#include <glm/glm.hpp>
#include <map>

#include <ft2build.h>
#include <iostream>
#include FT_FREETYPE_H

namespace TTH {

    /**
     * \brief A class containing font data.
     *
     * Use the ResourceManager to load fonts.
     *
     * This class should not be used directly.
     */
    class Font {
    public:
        /**
         * A struct representing a single character from the font.
         */
        struct Character{
            unsigned int TextureID;
            glm::ivec2 Size;
            glm::ivec2 Bearing;
            long Advance;
        };

        /**
         * Returns the font's glyph that represents a given character.
         * If no glyph is found, returns a default Character not suitable for rendering (with TextureID equal to 0)
         *
         * @param c - The character to return.
         * @return A Character struct with the requested qlyph data.
         */
        Character operator[](char c);

        /**
         * Returns the font's glyph that represents a given character.
         * If no glyph is found, throws an std::out_of_range exception.
         *
         * @param c - The character to return.
         * @return A Character struct with the requested qlyph data.
         */
        Character at(char c) const;
        Font(const std::string& font, unsigned int fontSize);
    private:
        std::map<char, Character> characters;
    };
}


#endif //THETETRAHEDRONTEST_FONT_H


#ifndef THETETRAHEDRON_MESH_H
#define THETETRAHEDRON_MESH_H

#include <assimp/Importer.hpp>
#include <glm/glm.hpp>
#include <vector>
#include "Shader.h"

namespace TTH {
    /**
     * \brief A class representing a mesh.
     *
     * This class is used by the engine internally. Try using a RenderComponent instead to load and render 3D objects automatically.
     *
     * When a Mesh is drawn, the shader used gets data about it's textures.
     * A mesh can have many textures of different types. They all should have a corresponding variable in the shaders.
     *
     * Supported texture types are: diffuse, specular, normal, height.
     *
     *     uniform sampler2D texture_diffuse1;
     *     uniform sampler2D texture_diffuse2;
     *     ...
     *     uniform sampler2D texture_specular1;
     *     uniform sampler2D texture_specular2;
     *     ...
     *     uniform sampler2D texture_normal1;
     *     ...
     *     uniform sampler2D texture_height1;
     *     ...
     *
     */
    class Mesh {
    public:
        struct Vertex{
            glm::vec3 Position = glm::vec3(0.0f);
            glm::vec2 TexCoords = glm::vec2(0.0f);
            glm::vec3 Normal = glm::vec3(0.0f);
            glm::vec3 Tangent = glm::vec3(0.0f);
            glm::vec3 Bitangent = glm::vec3(0.0f);
        };

        struct Tex{
            unsigned int id;
            std::string type;
            aiString path;
        };

        Mesh(const std::vector<Vertex>& vertices, const std::vector<unsigned int>& indices, const std::vector<Tex>& textures);

        /**
         * Draws the mesh
         * @param shader - The shader used for drawing
         */
        void Draw(const Shader& shader) const;
    private:
        unsigned int VAO, VBO, EBO;

        void SetupDraw(const Shader& shader) const;
        void SetupMesh();

        std::vector<Vertex> vertices;
        std::vector<unsigned int> indices;
        std::vector<Tex> textures;
    };
}

#endif //THETETRAHEDRON_MESH_H

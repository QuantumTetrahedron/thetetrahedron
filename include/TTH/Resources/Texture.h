
#ifndef THETETRAHEDRONTEST_TEXTURE_H
#define THETETRAHEDRONTEST_TEXTURE_H

#include <iostream>

namespace TTH {
    /**
     * \brief A class containing a texture data.
     *
     * This class is used internally by the engine and can be ignored.
     * Use the SpriteComponent to render textures on the screen.
     */
    class Texture {
    public:
        Texture() = default;
        ~Texture();

        /**
         * Properly generates a texture loading data from a file
         * @param path - The texture file
         */
        explicit Texture(const char* path);

        /**
        * Wraps a previously generated texture id to the texture class.
        * @param _id - The id of the texture already generated.
        */
        explicit Texture(unsigned int _id);

        /**
         * Binds the texture.
         */
        void Bind() const;

        unsigned int id;
    private:
        int width, height, nrChannels;
        unsigned int dataFormat, internalFormat;
        unsigned int wrapS, wrapT, filterMin, filterMag;
    };
}


#endif //THETETRAHEDRONTEST_TEXTURE_H

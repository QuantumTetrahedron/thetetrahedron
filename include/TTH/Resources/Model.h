
#ifndef THETETRAHEDRON_MODEL_H
#define THETETRAHEDRON_MODEL_H

#include "Mesh.h"

#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

namespace TTH {

    /**
     * \brief A class representing a 3D object
     *
     * This class is used by the engine internally. Try using a RenderComponent instead to load and render 3D objects automatically.
     *
     * A model is basically a collection of meshes.
     *
     * \todo Skeletal animations.
     */
    class Model {
    public:
        explicit Model(const std::string& filePath);

        /**
         * Draws the model.
         * @param shader - the shader to use.
         */
        void Draw(const Shader& shader) const;
    private:
        void LoadModel(const std::string& filePath);
        void ProcessNode(aiNode* node, const aiScene* scene);
        Mesh ProcessMesh(aiMesh* mesh, const aiScene* scene);

        std::vector<Mesh::Tex> LoadMaterialTextures(aiMaterial* mat, aiTextureType type, std::string typeName, bool gammaCorrection);
        unsigned int TextureFromFile(const char* path, const std::string& directory, bool gammaCorrection);

        std::vector<Mesh::Tex> textures;
        std::vector<Mesh> meshes;
        std::string directory;
    };
}

#endif //THETETRAHEDRON_MODEL_H

//
// Created by bartek on 21.12.2019.
//

#ifndef THETETRAHEDRON_UNIFORMBUFFER_H
#define THETETRAHEDRON_UNIFORMBUFFER_H

#include "Shader.h"

namespace TTH {
    class UniformBuffer {
    public:
        UniformBuffer(const std::string& name, unsigned int size);
        void LinkShader(Shader* shader);

        void SetInt(int value, unsigned int offset);
        void SetFloat(float value, unsigned int offset);
        void SetVec2(glm::vec2 value, unsigned int offset);
        void SetVec3(glm::vec3 value, unsigned int offset);
        void SetVec4(glm::vec4 value, unsigned int offset);
        void SetMat4(glm::mat4 value, unsigned int offset);

    private:
        std::string name;

        unsigned int ubo;
        unsigned int bindingPoint;
        static unsigned int nextId;
    };
}

#endif //THETETRAHEDRON_UNIFORMBUFFER_H

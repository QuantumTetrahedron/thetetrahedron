
#ifndef THETETRAHEDRONTEST_TEXTRENDERER_H
#define THETETRAHEDRONTEST_TEXTRENDERER_H

#include <string>

namespace TTH {

    class Font;

    /**
     * \brief A class for rendering text.
     *
     * The TextRenderer is used internally by the engine. No need to use it manually.
     * To render a text, create an Object with a TextComponent attached.
     */
    class TextRenderer {
    public:
        /**
         * Initializes the renderer.
         */
        static void Init();

        /**
         * Draws the requested text.
         * This method is not aware of the shader used.
         * Sends data to the shader as a vec4, where the x and y values are the vertex position,
         * and the z and w are its texture coordinates.
         * Use this line in a shader used for text:
         *
         *     layout (location = 0) in vec4 vertex;
         *
         * @param text - The text to write.
         * @param font - The font to use.
         */
        static void Draw(const std::string& text, Font* font);
    private:
        static unsigned int VAO, VBO;
    };
}


#endif //THETETRAHEDRONTEST_TEXTRENDERER_H

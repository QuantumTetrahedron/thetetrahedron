
#ifndef THETETRAHEDRON_GFX_H
#define THETETRAHEDRON_GFX_H

#include <vector>
#include <stack>
#include "RenderComponent.h"
#include "TextRenderer.h"
#include "LightComponent.h"
#include "TTH/Utils/PausableVector.h"
#include "TTH/Utils/PausableSortedList.h"
#include "TTH/UI/HUDComponent.h"

namespace TTH {

    class CameraComponent;
    //class HUDComponent;
    class Framebuffer;
    class Texture;

    /**
     * The Graphics engine.
     *
     * \todo zIndex for HUD.
     * \todo Maybe a DrawFrom method that uses a different camera.
     */
    class Gfx {
    public:

        /**
         * Draws every registered RenderComponent and HUDComponent.
         *
         * Fills the following variables in shaders used for RenderComponents:
         * - mat4 projection - The projection matrix.
         * - mat4 view - The view matrix.
         * - mat4 projectionView - Premultiplied projection and view matrices.
         * - vec3 viewPos - Position of the camera in world space.
         * Also the data from each LightComponent is passed. See LightComponent::SetUniforms().
         *
         * Fills the following variables in shaders used for HUDComponents:
         * - mat4 projection - The projection matrix.
         */
        static void Draw();

        static void DisplayFramebuffer(const Framebuffer& framebuffer);

        /**
         * Registers a RenderComponent. Only registered components are being drawn.
         * @param rc - The component to register.
         */
        static void RegisterComponent(RenderComponent* rc);

        /**
         * Registers a HUDComponent. Only registered component are being drawn.
         * @param hc - The component to register.
         */
        static void RegisterComponent(HUDComponent* hc);

        /**
         * Unregisters a RenderComponent.
         * @param rc - The component to unregister.
         */
        static void UnregisterComponent(RenderComponent* rc);

        /**
         * Unregisters a HUDComponent/
         * @param hc - The component to unregister.
         */
        static void UnregisterComponent(HUDComponent* hc);

        /**
         * Registers a LightComponent. These components are used to illuminate the RenderComponents.
         * @param lc - The component to register.
         */
        static void RegisterLight(LightComponent* lc);

        /**
         * Unregisters a LightComponent.
         * @param lc - The component to urregister.
         */
        static void UnregisterLight(LightComponent* lc);

        /**
         * Registers a camera. For now every unpaused active camera draws simultaneously on the screen. It's better to have just one on the scene.
         * @param cc - The CameraComponent to register.
         */
        static void RegisterCamera(CameraComponent* cc);

        /**
         * Unregisters a CameraComponent.
         * @param cc - The component to unregister.
         */
        static void UnregisterCamera(CameraComponent* cc);

        /**
         * Gets a pointer to the first camera found, marked as main (There should only ever be one anyway).
         * @return A pointer to the main camera.
         */
        static CameraComponent* GetMainCamera();

        /**
         * Gets the current screen resolution.
         * @param width - a pointer to an int to be filled with the window's width
         * @param height - a pointer to an int to be filled with the window's height
         */
        static void GetResolution(int* width, int* height);

        /**
         * Returns the last drawn frame as a texture
         * @return - a texture with the last frame drawn.
         */
        static Texture* GetLastFrameTexture();

        /**
         * Enables depth testing.
         */
        static void EnableDepthTest();

        /**
         * Disables depth testing.
         */
        static void DisableDepthTest();

        /**
         * Enables blending.
         */
        static void EnableBlending();

        /**
         * Disables blending.
         */
        static void DisableBlending();

        /**
         * Checks if depth testing is enabled
         */
        static bool DepthTestEnabled();

        static void SetResolution(int width, int height);

        static void Clear();
        static void DrawWorldSpace(CameraComponent* c);
        static void DrawHUDSpace(CameraComponent* c);
        static const PausableVector<RenderComponent*>& GetWorldSpaceComponents();

        static void UseFramebuffer(Framebuffer* fb);
        static void UseNoFramebuffer();
        static bool IsUsingFramebuffer();
        static Framebuffer* GetUsedFramebuffer();
    private:

        static Framebuffer* usedFramebuffer;

        static PausableVector<RenderComponent*> worldSpaceComponents;
        static PausableSortedList<HUDComponent*, HUDComponent::zIndexComparator> hudSpaceComponents;
        static PausableVector<CameraComponent*> cameras;
        static PausableVector<LightComponent*> lights;

        static int framebufferWidth, framebufferHeight;
        static Texture* lastFrame;

        static Object* Canvas;
        static void CreateCanvas();
        static void RemoveCanvas();

        friend class SceneManager;
        static void Pause();
        static void Resume();
        static void ClearData();


        friend class Game;
        static void Init();
        friend class Framebuffer;

        static bool depthTestingEnabled;
    };
}


#endif //THETETRAHEDRON_GFX_H

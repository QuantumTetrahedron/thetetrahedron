
#ifndef THETETRAHEDRON_RENDERCOMPONENT_H
#define THETETRAHEDRON_RENDERCOMPONENT_H

#include "TTH/Core/Component.h"

namespace TTH {

    class Model;
    class Shader;
    template<typename> class ComponentRegister;

    /**
     * \brief A class to render 3D models.
     *
     * An object with a RenderComponent attached will be rendered.
     *
     * The .ini file used to create this component should have the following parameters:
     *
     * - shader - the name of the shader to use.
     * - model - the name of the model to render.
     * - colorTint - a color tint.
     *
     * The following shader variables are set while drawing this component:
     *
     * - mat4 model - The model matrix.
     * - vec3 colorTint - The object's color tint.
     */
    class RenderComponent : public Component{
    public:
        RenderComponent();
        ~RenderComponent() override;

        /**
         * Creates a clone of this component. The cloned component is automatically registered in the Gfx class.
         * @return The created clone.
         */
        RenderComponent* Clone() const override;

        void SetColor(glm::vec3 new_color);
        void SetModel(const std::string& newModelName);
        void SetShader(const std::string& newShaderName);
        void SetShader(Shader* newShader);

        const Model* GetModel() const;
        Shader* GetShader() const;

        virtual void Draw();

        bool CastsShadows();
    protected:
        friend class Gfx;

        Model* model;
        Shader* shader;
        glm::vec3 colorTint;

        bool castsShadows;

        bool LoadFromFile(IniFile& file) override;
        void OnLoad() override;
    private:
        static ComponentRegister<RenderComponent> reg;
    };
}


#endif //THETETRAHEDRON_RENDERCOMPONENT_H

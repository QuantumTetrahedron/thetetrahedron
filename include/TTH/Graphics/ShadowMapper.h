//
// Created by bartek on 22.12.2019.
//

#ifndef THETETRAHEDRON_SHADOWMAPPER_H
#define THETETRAHEDRON_SHADOWMAPPER_H

#include <string>
#include <TTH/Resources/Shader.h>
#include <TTH/Core/CameraComponent.h>

namespace TTH {
    class ShadowMapper {
    public:
        class ShadowMap {
        public:
            void Bind() const;
            void Unbind() const;
            void Generate();
            void UpdateShader(Shader *shader, std::string name, int texNum);
            void SetLightSpaceMatrix(glm::mat4 lsm);
        private:
            glm::mat4 lightSpaceMatrix;
            unsigned int depthMapFBO, depthMap;
        };

        static void FillShadowMap(ShadowMap &shadowMap, glm::mat4 lightSpaceMatrix);
        static ShadowMap CreateNewShadowMap();
    };
}

#endif //THETETRAHEDRON_SHADOWMAPPER_H

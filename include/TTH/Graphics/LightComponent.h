
#ifndef THETETRAHEDRONTEST_LIGHTCOMPONENT_H
#define THETETRAHEDRONTEST_LIGHTCOMPONENT_H

#include "TTH/Core/Component.h"

namespace TTH {

    class Shader;
    template<typename> class ComponentRegister;

    /**
     * \brief A component used to add light to the scene.
     *
     * An object with a LightComponent attached will send it's position and a light color to the shaders.
     *
     * The .ini file used to create this component should have the following parameters:
     *
     * - name - the name for this light that will be used in the shaders
     * - color - the light's color in RGB color space
     *
     * To get the LampComponent data in a shader, you need to define a struct named Light and a variable of that type:
     *
     *     struct Light{
     *         vec3 position;
     *         vec3 color;
     *     };
     *
     *     Light lightName;
     */
    /*
    class LightComponent : public Component{
    public:
        LightComponent();
        ~LightComponent() override;

        / **
         * Creates a copy of the LightComponent. The copy is automatically registered in the Gfx class.
         * @return The created copy.
         * /
        LightComponent* Clone() const override;

        void SetColor(glm::vec3 new_color);
        glm::vec3 GetColor() const;
    protected:
        friend class Gfx;

        std::string name;
        glm::vec3 color;

        bool LoadFromFile(IniFile& file) override;
        void OnLoad() override;

        virtual void SetUniforms(Shader* shader);
    private:
        static ComponentRegister<LightComponent> reg;
    };*/

    class LightComponent : public Component{
    public:
        ~LightComponent() override;

        virtual void SetUniforms(Shader* shader) = 0;

        virtual bool CastsShadows() = 0;
        virtual void CreateShadowMap() = 0;
    protected:
        void OnLoad() override;
    };
}


#endif //THETETRAHEDRONTEST_LIGHTCOMPONENT_H


#ifndef THETETRAHEDRON_COMPONENT_H
#define THETETRAHEDRON_COMPONENT_H

#include "Object.h"

namespace TTH{

    /** \brief A base abstract class for every component
     *
     * Defines the basic functionality of every object.
     */
    class Component {
    public:
        Component();
        virtual ~Component() = default;

        /**
         * Used to make an exact copy of the component.
         * Override this method to specify which data should be copied.
         * @return A copy of this component.
         */
        virtual Component* Clone() const = 0;

        /**
         * Checks if the component has been marked for destruction.
         * @return true if the object is dead.
         */
        bool IsDead() const;

        /**
         * Marks the component for destruction.
         */
        void Kill();

        /**
         * Checks if the component is active.
         * @return true if the component is active.
         */
        bool IsActive() const;

        /**
         * Activates or deactivates the component
         * @param active - Should the component be active?
         */
        void SetActive(bool active);

        const Object* GetParent() const;

        friend class Object;
        friend class ObjectFactory;
    protected:

        virtual bool LoadFromFile(IniFile& file) = 0;
        virtual void OnLoad() {}
        virtual void OnActive() {}

        Object* parent;
        bool isDead;
        bool isActive;

        void SetParent(Object* p);
    };
}


#endif //THETETRAHEDRON_COMPONENT_H

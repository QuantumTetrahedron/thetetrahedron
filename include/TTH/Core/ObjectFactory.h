
#ifndef THETETRAHEDRON_OBJECTFACTORY_H
#define THETETRAHEDRON_OBJECTFACTORY_H

#include <map>
#include <string>
#include "ComponentFactory.h"
#include "TTH/Utils/IniFile.h"

namespace TTH {
    class ObjectFactory {
    public:
        static void SetArchetype(IniFile archetypeFile);
        static void RemoveArchetype();
        static Object* Create(IniFile& objectFile, const std::string& objectName);
    private:
        static bool useArchetype;
        static IniFile currArchetype;
    };
}


#endif //THETETRAHEDRON_OBJECTFACTORY_H


#ifndef THETETRAHEDRONTEST_COLLIDERCOMPONENT_H
#define THETETRAHEDRONTEST_COLLIDERCOMPONENT_H

#include "Component.h"
#include "ComponentFactory.h"
#include "BehaviourComponent.h"

namespace TTH {
    class ColliderComponent : public Component{
    public:
        ~ColliderComponent() override;
        ColliderComponent* Clone() const override;

        /**
         * Returns true if the object collides with a given point
         * @param point - The point to check
         * @return true if the object collides with it
         */
        bool Collides(glm::vec2 point);

        glm::vec3 GetScale();
        void SetScale(glm::vec3 newScale);
    protected:
        bool LoadFromFile(IniFile& file) override;
        void OnLoad() override;

        std::vector<BehaviourComponent*> components;
    private:
        friend class MouseHandler;
        friend class Ray;

        void OnMouseDown(int button);
        void OnMouseEnter();
        void OnMouseLeave();

        glm::vec3 scale;

        static ComponentRegister<ColliderComponent> reg;
    };
}

#endif //THETETRAHEDRONTEST_COLLIDERCOMPONENT_H

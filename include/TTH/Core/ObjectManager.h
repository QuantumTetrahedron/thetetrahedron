
#ifndef THETETRAHEDRONTEST_OBJECTMANAGER_H
#define THETETRAHEDRONTEST_OBJECTMANAGER_H

#include <stack>
#include "BehaviourComponent.h"
#include "TTH/Utils/PausableVector.h"

namespace TTH {

    /**
     * Manages in game objects
     */
    class ObjectManager {
    public:
        /**
         * Adds a new object to the manager
         * @param o - The object to add.
         */
        static void AddObject(Object* o);

        /**
         * Finds an object with the given name. Not very efficient, so avoid using in update.
         * @param name - The name of the object to find.
         * @return The object found or a nullptr.
         */
        static Object* GetObject(const std::string& name);

        /**
         * Destroys every non-paused object.
         */
        static void DestroyObjects();

        static std::vector<Object*> GetChildrenOf(Object* obj);
    private:
        friend class SceneManager;
        static PausableVector<Object*> objects;

        static void Init();
        static void Start();
        static void Update(float dt);
        static void Pause();
        static void Resume();
        static void DestroyAllObjects();
    };
}


#endif //THETETRAHEDRONTEST_OBJECTMANAGER_H

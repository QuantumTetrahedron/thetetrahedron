
#ifndef THETETRAHEDRONTEST_TRANSFORM_H
#define THETETRAHEDRONTEST_TRANSFORM_H

#include <glm/glm.hpp>

namespace TTH {
    /**
     * \brief A position, rotation and scale.
     *
     * That's basically it.
     */
    class Transform {
    public:
        glm::vec3 position;
        glm::vec3 rotation;
        glm::vec3 scale;

        /// 3D pivot for rotation. Usually (0,0,0) anyway unless it's a part of HUD
        glm::vec3 pivot;

        /**
         * @return A model matrix representing this transform
         */
        glm::mat4 GetModelMatrix() const;
    };
}


#endif //THETETRAHEDRONTEST_TRANSFORM_H


#ifndef THETETRAHEDRON_BEHAVIOURCOMPONENT_H
#define THETETRAHEDRON_BEHAVIOURCOMPONENT_H

#include "Component.h"

namespace TTH{

    /** \brief An interface for behavioural components
     *
     * Implementations of this interface give custom behaviours to your objects.
     * Most of a game logic can be done inside those components.
     *
     * For example, if you want a floating platform to jump on, just edit
     * it's position in the Update method.
     *
     * To fully implement this interface, some additional methods from the Component interface must also be implemented.
     * - bool LoadFromFile(IniFile& file) - To allow loading data from .ini files.
     * - YourComponent* Clone() const - To allow making an exact copy of any object at any time.
     * - void OnLoad() - optional - Will be called after the component is created and attached to it's parent. Is guaranteed to be called only once during the object's lifespan. Should not interact with other, possibly not yet created objects.
     * - void OnActive() - optional - Allows to take an action after the component's activation. Also called on creation after OnLoad(), but components can be toggled in runtime, so this function is not guaranteed to be called just once.
     * - void OnMouseDown() - optional - To execute certain actions when the object is clicked. For an object to register the click event, it must contain a ColliderComponent or HUDColliderComponent.
     * - void OnMouseEnter() - optional - Executes once each time the cursor enters the area of the screen occupied by this object.
     * - void OnMouseLeave() - optional - Executes once each time the cursor leaves the area of the screen occupied by this object.
     *
     *
     * @see Component
     * @see Object
     */
    class BehaviourComponent : public Component{
    public:
        /**
         * Start is called once, after the creation of any scene featuring an object with this component.
         * At this point every other object has already been created and everything is ready to work.
         */
        virtual void Start() = 0;

        /**
         * Update is called every frame.
         * @param dt - Delta time measures the time that passed since last frame.
         */
        virtual void Update(float dt) = 0;

        /**
         * OnLeave is called once, when the scene containing the parented object is destroyed.
         */
        virtual void OnLeave() = 0;

        /**
         * Called when an object with this component is clicked. The object must have either a ColliderComponent or
         * a HUDColliderComponent attached in order to call this method. When an object with a ColliderComponent (or HUDColliderComponent)
         * is clicked, every BehaviourComponent attached to that object will call the OnMouseDown method.
         */
        virtual void OnMouseDown(int button) {}

        /**
         * Executes once each time the cursor enters the area of the screen occupied by this object.
         * Also called only if the object has a ColliderComponent (or HUDColliderComponent) attached.
         */
        virtual void OnMouseEnter(){}

        /**
         * Executes once each time the cursor leaves the area of the screen occupied by this object.
         * Also called only if the object has a ColliderComponent (or HUDColliderComponent) attached.
         */
        virtual void OnMouseLeave(){}
    };
}

#endif //THETETRAHEDRON_BEHAVIOURCOMPONENT_H

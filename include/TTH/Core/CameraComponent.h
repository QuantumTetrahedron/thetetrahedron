
#ifndef THETETRAHEDRON_CAMERACOMPONENT_H
#define THETETRAHEDRON_CAMERACOMPONENT_H

#include "ComponentFactory.h"

#include <glm/glm.hpp>

namespace TTH {

    class Texture;
    class Framebuffer;

    /** \brief A component needed to display anything on a screen.
     *
     * In order to render anything on screen, at least one scene object must have this component attached.
     *
     * This component assumes that the world's up vector is (0,1,0) and ignores the roll.
     * \todo Make the camera more free. This requires defining the rotations as quaternions.
     *
     * The .ini file used to create this component should have the following parameters:
     *
     * - screenWidth - width of the screen or framebuffer the camera is drawing to.
     * - screenHeight - height of the screen or framebuffer the camera is drawing to.
     * - fieldOfView - field of view in degrees
     * - mainCamera - whether it should be automatically registered as main during creation.
     *
     * @see Component
     * @see Object
     */
    class CameraComponent : public Component {
    public:
        CameraComponent();
        ~CameraComponent() override;

        /**
         * Gets the last frame drawn by this camera, whether it was on or off screen.
         * To get the last displayed frame you can also use Gfx::GetLastFrameTexture()
         * @return the texture drawn by this camera in the previous frame.
         */
        Texture* GetLastFrameTexture();

        /**
         * Gets the vector pointing in front of the camera.
         * @return The front vector.
         */
        glm::vec3 GetFront() const;

        /**
         * Gets the screen's width, specified earlier in a config file.
         * @return The screen's width.
         */
        int GetWidth() const;

        /**
         * Gets the screen's height, specified earlier in a config file.
         * @return The screen's height.
         */
        int GetHeight() const;

        /**
         * Adds a postprocessing effect to the texture rendered by this camera.
         * @param name - the name of the effect to use.
         *
         * @see Postprocessing for a list of available effects and instructions to create a custom one.
         */
        void AddPostprocessEffect(const std::string& name);

        /**
         * Removes a postprocessing effect. Will only remove the first found effect with that name.
         * @param name - the name of the effect to remove.
         */
        void RemovePostprocessEffect(const std::string& name);

        /**
         * Creates a copy of the camera. The new CameraComponent is not registered as main.
         * @return A copy of the CameraComponent.
         */
        CameraComponent* Clone() const override;

        glm::vec3 GetPosition();
        glm::mat4 GetViewMatrix();
        glm::mat4 GetProjectionMatrix();
        glm::mat4 GetHUDProjectionMatrix();

        void Use();
        void EndUse();

        void SetResolution(int w, int h);
        void SetFront(glm::vec3 newFront);
        Framebuffer* framebuffer;
        void Display();
        glm::vec3 backgroundColor;
    protected:
        friend class Gfx;
        friend class MouseHandler;


        bool LoadFromFile(IniFile& file) override;
        void OnLoad() override;

        const glm::vec3 worldUp;

        bool isMain;
        bool drawWorldSpace, drawHUDSpace;

        float fieldOfView;
        int screenWidth;
        int screenHeight;
        float aspectRatio;


        glm::vec3 front;

        Texture* lastFrameTexture;

        std::vector<std::string> postprocessEffects;
    private:
        static ComponentRegister<CameraComponent> reg;
    };
}


#endif //THETETRAHEDRON_CAMERACOMPONENT_H

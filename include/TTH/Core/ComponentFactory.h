
#ifndef THETETRAHEDRON_COMPONENTFACTORY_H
#define THETETRAHEDRON_COMPONENTFACTORY_H

#include "Component.h"
#include <map>

namespace TTH {
    template<typename T>
    Component* createT(){return new T;}

    /** \brief A factory for components.
     *
     * The component factory makes it possible to create a component of a given type
     * by passing a string containing the class name.
     */
    class ComponentFactory {
    public:
        typedef std::map<std::string, Component*(*)()> mapType;

        /**
         * Creates a component of the requested type.
         * @param className - the name of the requested class
         * @return A new component of the requested class
         */
        static Component *create(const std::string &className);

    protected:
        static mapType* getMap();
    private:
        static mapType* map;
    };

    /**
     * To make the creation of a component possible using the factory, the class must be registered in the factory's mapping.
     * Each component should include the following code:
     *
     * In the .h file
     *
     *     static TTH::ComponentRegister<YourComponentClass> reg;
     *
     * In the .cpp file
     *
     *     TTH::ComponentRegister<YourComponentClass> YourComponentClass::reg("YourComponentClass");
     */
    template<typename T>
    class ComponentRegister : public ComponentFactory{
    public:
        explicit ComponentRegister(const std::string& className){
            getMap()->insert(std::make_pair(className, &createT<T>));
        }
    };
}

#endif //THETETRAHEDRON_COMPONENTFACTORY_H


#ifndef THETETRAHEDRON_OBJECT_H
#define THETETRAHEDRON_OBJECT_H

#include "Transform.h"
#include "TTH/Utils/IniFile.h"
#include <vector>
#include <string>
#include <glm/glm.hpp>

namespace TTH {

    class Component;
    class BehaviourComponent;

    /** \brief A class representing every in-game object
     *
     * An Object is generally a transform with components attached.
     * It can be parented to another object.
     */
    class Object {
    public:
        Object();
        ~Object();

        /**
         * Adds a new component to the object
         * @param c - The component to add.
         */
        void AddComponent(Component* c);

        /**
         * Gets a component of a given type attached to this object
         * @tparam T - the requested component type.
         * @return the first component of the specified type that is found.
         */
        template <typename T>
        T* GetComponent();

        /**
         * Gets every component of a given type attached to this object.
         * @tparam T - the requested component type.
         * @return a vector of components of the specified type.
         */
        template <typename T>
        std::vector<T*> GetAllComponents();

        /**
         * Returns the object's global transform in world space, considering it's parent position.
         *
         * @return the object's global transform.
         */
        Transform GetWorldTransform() const;

        /**
         * The object's local transform in world space.
         */
        Transform transform;

        /**
         * Checks if the parent object has been killed.
         * @return true if the object is dead.
         */
        bool IsDead() const;

        /**
         * Creates a perfect clone of the object. All the attached components are also cloned.
         * @return The created clone.
         */
        virtual Object* Clone() const;

        /**
         * Sets a parent for the object. The object's location will be relative to it's parent.
         * @param o - A pointer to the parent object.
         */
        void SetParent(Object* o);

        /**
         * Checks whether the object has a parent attached.
         * @return true if the object has a parent.
         */
        bool HasParent() const;

        /**
         * Checks if the object is active.
         * @return true if the object is active.
         */
        bool IsActive() const;

        /**
         * Activates or deactivates an object.
         * @param active - Should the object be active?
         */
        void SetActive(bool active);

        /**
         * Gets the object's name
         * @return the object's name
         */
        std::string GetName() const;

        void Kill();
    private:
        std::vector<Component*> components;
        std::vector<BehaviourComponent*> behaviours;
        Object* parent;

        bool isDead;
        bool isActive;

        friend class ObjectManager;
        void Start();
        void Update(float dt);
        void OnActive();
        void OnDestroy();
        void RemoveAllComponents();

        void RemoveComponent(Component* c);

        friend class ObjectFactory;
        void LoadFromFile(IniFile& file);

        std::string name;
    };

    template<typename T>
    T *Object::GetComponent() {
        for(Component* c : components)
        {
            T* cast = dynamic_cast<T*>(c);
            if(cast)
                return cast;
        }
        return nullptr;
    }

    template<typename T>
    std::vector<T *> Object::GetAllComponents() {
        std::vector<T*> ret;
        for(Component* c : components){
            T* cast = dynamic_cast<T*>(c);
            if(cast)
                ret.push_back(cast);
        }
        return ret;
    }
}


#endif //THETETRAHEDRON_OBJECT_H

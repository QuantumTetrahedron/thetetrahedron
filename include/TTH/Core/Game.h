#ifndef THETETRAHEDRON_GAME_H
#define THETETRAHEDRON_GAME_H

#include <GLFW/glfw3.h>
#include <string>
#include <TTH/Input/Input.h>


namespace TTH {
    class Window;

    /** \brief The main class of the engine
     *
     * This class is a simple to use facade for the engine.
     * To run the engine appropiately you just need 3 lines of code.
     *
     *     TTH::Game::Initialize("/GameData/InitData.ini");
     *     TTH::Game::MainLoop();
     *     TTH::Game::Shutdown();
     *
     */
    class Game {
    public:
        /**
         * Call Initialize to prepare all the necessary data and set the configuration options.
         *
         * @param configFilePath - A file with game config. See /GameData/InitData.ini for an example
         */
        static void Initialize(const char* configFilePath);

        /**
         * Call this method once to enter the game's main loop.
         * The function returns when the game's window is closed.
         */
        static void MainLoop();

        /**
         * This method should be called after leaving the main loop in order to properly clean everything.
         */
        static void Shutdown();

        static Window window;
        static int getFPS();
    private:

        static void LoadWindow(const std::string& windowOptionsPath);
        static void LoadRenderingOptions(const std::string& renderingOptionsPath);
        static void LoadResources(const std::string& resourceOptionsPath);
        static void LoadInput(const std::string& inputOptionsPath);
        static void LoadGame(const std::string& gameOptionsPath);

        static float deltaTime;
        static float lastFrameTime;

        static int fps, lastFPS;
    };
}


#endif //THETETRAHEDRON_GAME_H

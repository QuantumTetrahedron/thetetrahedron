
#ifndef THETETRAHEDRONTEST_TEXTCOMPONENT_H
#define THETETRAHEDRONTEST_TEXTCOMPONENT_H

#include "HUDComponent.h"

namespace TTH {

    class Font;
    template<typename> class ComponentRegister;

    /**
     * \brief A 2D text to render.
     *
     * A component representing a 2D text. An object with a TextComponent will be rendered as a 2D text in screen space.
     *
     * The .ini file used to create this component should have the following parameters:
     *
     * - text - The text to write on screen
     * - font - The name of the font to use
     * - colorTint - The color of the text.
     * - shader - The name of the shader to use.
     * - anchor - A 2D point on the object's parent, relative to which this object's position is calculated.
     *
     * The following shader variables are set while drawing this component:
     *
     * - vec3 colorTint - the sprite's color tint.
     * - mat4 model - the model matrix.
     * - Sampler2D text - the texture with the text to render.
     *
     * \todo Check if the text fits in the designated area, draw in next line if not
     * \todo Add alignment parameter
     */
    class TextComponent : public HUDComponent{
    public:
        ~TextComponent() override;

        /**
         * Creates a clone of this component. The cloned component is automatically registered in the Gfx class.
         * @return The created clone.
         */
        Component* Clone() const override;

        void SetText(const std::string& newText);
        void SetColor(glm::vec3 newColor);
        void SetFont(const std::string& fontName);

        friend class Gfx;
    protected:
        std::string text;
        glm::vec3 colorTint;
        Font* font;

        void Draw() override;
        bool LoadFromFile(IniFile& file) override;
        void OnLoad() override;
    private:
        static ComponentRegister<TextComponent> reg;
    };
}


#endif //THETETRAHEDRONTEST_TEXTCOMPONENT_H

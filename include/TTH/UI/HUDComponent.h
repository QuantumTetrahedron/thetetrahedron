
#ifndef THETETRAHEDRONTEST_HUDCOMPONENT_H
#define THETETRAHEDRONTEST_HUDCOMPONENT_H

#include <glm/gtc/matrix_transform.hpp>
#include "TTH/Core/Component.h"

namespace TTH {

    class Shader;

    /** \brief An abstract class for HUD elements
     *
     * Every object with a component deriving from HUDComponent is being drawn after drawing every other non-HUD component.
     *
     * The component can be anchored to any point of its parent. The position of the object is then
     * relative to the anchor point. The anchor point is a 2D vector with the components in range <0,1>.
     * The value [0;0] points at top left corner of the parent, the value [1;1] points at the bottom right corner.
     *
     * A HUDComponent with no parent is automatically parented to a Canvas when registered for rendering.
     * That canvas has the size of the window created.
     *
     * The .ini file used to create this component should have the following parameters:
     *
     * - anchor - The point to anchor the object to.
     *
     * \todo ButtonComponent
     */
    class HUDComponent : public Component {
    public:
        /**
         * Sets the anchor to a new one
         * @param newAnchor - the new anchor
         * @param keepPosition - should the on screen position stay the same or be recalculated with the use of the new anchor?
         */
        void SetAnchor(glm::vec2 newAnchor, bool keepPosition);

        /**
         * Gets the anchor position
         * @return the anchor
         */
        glm::vec2 GetAnchor() const;

        /**
         * Returns the coordinates of the top left corner of the object.
         * @return screen space position.
         */
        glm::vec2 GetScreenPosition() const;

        void SetShader(const std::string& newShaderName);

        friend class Gfx;
    protected:

        virtual void Draw() = 0;
        bool LoadFromFile(IniFile& file) override;
        void OnLoad() override;

        Shader* shader;
        glm::vec2 anchor;

    private:
        struct zIndexComparator{
            bool operator()(HUDComponent* first, HUDComponent* second){
                return first->parent->transform.position.z < second->parent->transform.position.z;
            }
        };
    };
}


#endif //THETETRAHEDRONTEST_HUDCOMPONENT_H

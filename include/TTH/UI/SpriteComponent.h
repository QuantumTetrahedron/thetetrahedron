
#ifndef THETETRAHEDRONTEST_SPRITECOMPONENT_H
#define THETETRAHEDRONTEST_SPRITECOMPONENT_H

#include "HUDComponent.h"

namespace TTH {

    class Texture;
    template<typename> class ComponentRegister;

    /**
     * \brief A sprite.
     *
     * A component representing a sprite. An object with a SpriteComponent will be rendered as a 2D sprite in screen space.
     *
     * The .ini file used to create this component should have the following parameters:
     *
     * - shader - The name of the shader to use.
     * - texture - The name of the texture to render.
     * - colorTint - A color tint.
     * - anchor - A 2D point on the object's parent, relative to which this object's position is calculated.
     *
     * The following shader variables are set while drawing this component:
     *
     * - vec3 colorTint - the sprite's color tint.
     * - mat4 model - the model matrix.
     * - Sampler2D tex - the texture to render.
     */
    class SpriteComponent : public HUDComponent{
    public:
        ~SpriteComponent() override;

        /**
         * Creates a clone of this component. The cloned component is automatically registered in the Gfx class.
         * @return The created clone.
         */
        Component* Clone() const override;

        /**
         * Assigns a new texture to the component
         * @param newTex - the texture to assign.
         */
        void SetTexture(Texture* newTex);

        /**
         * Assigns a new texture to the component
         * @param newTexName - the name of the texture to assign
         */
        void SetTexture(std::string newTexName);

        /**
         * Assigns a new color tint to the component.
         * @param newTint - the new color tint.
         */
        void SetColorTint(glm::vec3 newTint);

        friend class Gfx;
    protected:
        Texture* texture;
        glm::vec3 colorTint;

        void Draw() override;
        bool LoadFromFile(IniFile& file) override;
        void OnLoad() override;
    private:
        static ComponentRegister<SpriteComponent> reg;
        static unsigned int VAO;
    };
}

#endif //THETETRAHEDRONTEST_SPRITECOMPONENT_H

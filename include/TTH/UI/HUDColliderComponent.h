
#ifndef THETETRAHEDRONTEST_BUTTONCOMPONENT_H
#define THETETRAHEDRONTEST_BUTTONCOMPONENT_H

#include "HUDComponent.h"

namespace TTH {

    template<typename> class ComponentRegister;

    /**
     * A class for collisions in HUD space. Currently the engine does not calculate any physics so both this component,
     * and the ColliderComponent used in World space serve only the purpose of checking collisions with
     * a point (in HUD space) or a ray (in World space). This mechanism is used, for example, when calculating
     * whether an object has been clicked. Remember to include this component in the objects that you want to interact with
     * using the Raycaster class.
     */
    class HUDColliderComponent : public Component{
    public:
        ~HUDColliderComponent() override;
        HUDColliderComponent* Clone() const override;

        /**
         * Returns true if the object collides with a given point
         * @param point - The point to check
         * @return true if the object collides with it
         */
        bool Collides(glm::vec2 point);


    protected:
        bool LoadFromFile(IniFile& file) override;
        void OnLoad() override;

        std::vector<BehaviourComponent*> components;
    private:
        friend class MouseHandler;

        void OnMouseDown(int button);
        void OnMouseEnter();
        void OnMouseLeave();

        static ComponentRegister<HUDColliderComponent> reg;
    };
}

#endif //THETETRAHEDRONTEST_BUTTONCOMPONENT_H

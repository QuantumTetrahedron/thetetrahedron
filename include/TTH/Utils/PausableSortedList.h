
#ifndef THETETRAHEDRONTEST_SORTEDPAUSABLELINKEDLIST_H
#define THETETRAHEDRONTEST_SORTEDPAUSABLELINKEDLIST_H

#include <stack>

template<typename T, class Predicate>
class PausableSortedList {
private:
    struct Node{
        Node(){
            value = T();
            next = nullptr;
        }

        explicit Node(T v){
            value = v;
            next = nullptr;
        }

        Node* next;
        T value;
    };

    unsigned int _size;
    Node* head;
    std::stack<Node*> pauseStack;
    Node sentinel;

    void RecalculateSize(){
        unsigned int newSize = 0;
        for(Node* it = head; it != &sentinel; it = it->next){
            newSize++;
        }
        _size = newSize;
    }
public:
    struct iterator{
        Node* node;
        iterator(Node* n){
            node = n;
        }

        T operator*() const {
            return node->element;
        }

        void operator++(){
            node = node->next;
        }

        bool operator==(const iterator& other) const {
            return node == other.node;
        }

        bool operator!=(const iterator& other) const {
            return node != other.node;
        }
    };

    PausableSortedList(){
        _size = 0;
        sentinel = Node();
        head = &sentinel;
    }

    ~PausableSortedList(){
        clearAll();
    }

    void Add(T value){
        if(empty()){
            Node* n = new Node(value);
            n->next = head;
            head = n;
        } else {
            for(Node* it = head; it != &sentinel; it = it->next){
                Predicate predicate;
                if(it->next == &sentinel || predicate(value, it->next->value)){
                    Node* n = new Node(value);
                    n->next = it->next;
                    it->next = n;
                    break;
                }
            }
        }
        _size++;
    }

    void Remove(T value){
        Node* prev = nullptr;
        for(Node* it = head; it != &sentinel; it = it->next){
            if(it->value == value){
                if(prev)
                    prev->next = it->next;

                if(it == head){
                    head = it->next;
                }

                delete it;
                _size--;
                break;
            }
            prev = it;
        }
    }

    bool empty(){
        return _size==0;
    }

    int size(){
        return _size;
    }

    void clear(){
        if(head != &sentinel){
            Node* n = head;
            Node* t = n->next;
            for(;t != &sentinel; n = t){
                t = n->next;
                delete n;
            }
        }
        head = &sentinel;
        _size = 0;
    }

    void clearAll(){
        while(!pauseStack.empty()) {
            head = pauseStack.top();
            pauseStack.pop();
        }
        clear();
    }

    iterator begin(){
        return iterator(head);
    }

    iterator end() {
        return iterator(&sentinel);
    }

    void Pause(){
        pauseStack.push(head);
        head = &sentinel;
        _size = 0;
    }

    void Resume(){
        clear();
        if(!pauseStack.empty()) {
            head = pauseStack.top();
            pauseStack.pop();
        }
        RecalculateSize();
    }
};

template<typename T, class Predicate>
class PausableSortedList<T*, Predicate>{
private:
    struct Node{
        Node(){
            value = nullptr;
            next = nullptr;
        }

        explicit Node(T* v){
            value = v;
            next = nullptr;
        }

        Node* next;
        T* value;
    };

    unsigned int _size;
    Node* head;
    std::stack<Node*> pauseStack;
    Node sentinel;

    void RecalculateSize(){
        unsigned int newSize = 0;
        for(Node* it = head; it != &sentinel; it = it->next){
            newSize++;
        }
        _size = newSize;
    }
public:
    struct iterator{
        Node* node;
        iterator(Node* n){
            node = n;
        }

        T* operator*() const {
            return node->value;
        }

        void operator++(){
            node = node->next;
        }

        bool operator==(const iterator& other) const {
            return node == other.node;
        }

        bool operator!=(const iterator& other) const {
            return node != other.node;
        }
    };

    PausableSortedList(){
        _size = 0;
        sentinel = Node();
        head = &sentinel;
    }

    ~PausableSortedList(){
        ClearAll();
    }

    void Add(T* value){
        if(empty()){
            Node* n = new Node(value);
            n->next = head;
            head = n;
        } else {
            for(Node* it = head; it != &sentinel; it = it->next){
                Predicate predicate;
                if(it->next == &sentinel || predicate(value, it->next->value)){
                    Node* n = new Node(value);
                    n->next = it->next;
                    it->next = n;
                    break;
                }
            }
        }
        _size++;
    }

    void Remove(T* value){
        Node* prev = nullptr;
        for(Node* it = head; it != &sentinel; it = it->next){
            if(it->value == value){
                if(prev)
                    prev->next = it->next;

                if(it == head){
                    head = it->next;
                }

                delete it;
                _size--;
                break;
            }
            prev = it;
        }
    }

    bool empty(){
        return _size==0;
    }

    int size(){
        return _size;
    }

    void clear(){
        if(head != &sentinel){
            Node* n = head;
            Node* t = n->next;
            for(;t != &sentinel; n = t){
                t = n->next;
                delete n;
            }
        }
        head = &sentinel;
        _size = 0;
    }

    void ClearAll(){
        while(!pauseStack.empty()) {
            head = pauseStack.top();
            pauseStack.pop();
        }
        clear();
    }

    void MergeAll(){
        while(!pauseStack.empty()){
            Node* b = pauseStack.top();
            Node* n = b;
            while(n->next != &sentinel){
                n = n->next;
            }
            n->next = head;
            head = b;
        }
    }

    iterator begin(){
        return iterator(head);
    }

    iterator end() {
        return iterator(&sentinel);
    }

    void Pause(){
        pauseStack.push(head);
        head = &sentinel;
        _size = 0;
    }

    void Resume(){
        if(!pauseStack.empty()) {
            head = pauseStack.top();
            pauseStack.pop();
        }
        RecalculateSize();
    }
};

#endif //THETETRAHEDRONTEST_SORTEDPAUSABLELINKEDLIST_H

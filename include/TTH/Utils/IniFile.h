
#ifndef THETETRAHEDRON_INIFILE_H
#define THETETRAHEDRON_INIFILE_H

#include <iostream>
#include <string>
#include <sstream>
#include <map>
#include <glm/glm.hpp>

namespace TTH {
    class IniFile {
    public:
        IniFile();
        void ReadFile(const char* filePath);
        IniFile UpdateWith(IniFile& other);

        template <typename T>
        bool GetValue(const std::string& key, T& value) const;

        template <typename T>
        bool GetValue(const std::string& key, T& value, const T& def) const;

        template <typename T>
        void RequireValue(const std::string& key, T& value) const;

        void SetToSection(const std::string& sectionName);

    private:
        void RemoveLeadingSpaces(const std::string& line, size_t& begin, size_t& end) const;
        void RemoveTrailingSpaces(const std::string& line, size_t& begin, size_t& end) const;
        void GetSectionName(std::string& line, size_t& begin, size_t& end) const;
        void ParseSectionName(std::pair<std::string, std::map<std::string, std::string>>& currSection, std::string& line, size_t& begin, size_t& end);
        void ParseValue(std::pair<std::string, std::map<std::string, std::string>>& currSection, std::string& line, size_t& begin, size_t& end);

        std::map<std::string, std::map<std::string, std::string>> sections;
        std::map<std::string, std::string>* currentSection;
    };


    template<typename T>
    bool IniFile::GetValue(const std::string &key, T& value) const {
        auto i = currentSection->find(key);
        if(i == currentSection->end()){
            return false;
        }
        std::stringstream ss(i->second);
        ss >> value;
        return true;
    }

    template<>
    inline bool IniFile::GetValue(const std::string &key, std::string& value) const {
        auto i = currentSection->find(key);
        if(i==currentSection->end()){
            return false;
        }
        value = i->second;
        return true;
    }

    template<>
    inline bool IniFile::GetValue(const std::string &key, glm::vec2 &value) const {
        auto i = currentSection->find(key);
        if(i==currentSection->end()){
            return false;
        }
        std::string vecStr = i->second;
        std::stringstream ss(vecStr);
        ss >> value.x >> value.y;
        return true;
    }

    template<>
    inline bool IniFile::GetValue(const std::string &key, glm::vec3 &value) const {
        auto i = currentSection->find(key);
        if(i==currentSection->end()){
            return false;
        }
        std::string vecStr = i->second;
        std::stringstream ss(vecStr);
        ss >> value.x >> value.y >> value.z;
        return true;
    }

    template<>
    inline bool IniFile::GetValue(const std::string &key, glm::vec4 &value) const {
        auto i = currentSection->find(key);
        if(i==currentSection->end()){
            return false;
        }
        std::string vecStr = i->second;
        std::stringstream ss(vecStr);
        ss >> value.x >> value.y >> value.z >> value.w;
        return true;
    }

    template <typename T>
    bool IniFile::GetValue(const std::string &key, T &value, const T& def) const {
        if(!GetValue(key, value)){
            value = def;
            return true;
        }
        return false;
    }

    template <typename T>
    void IniFile::RequireValue(const std::string &key, T &value) const {
        if(!GetValue(key, value)){
            throw std::runtime_error("Required value " + key + " not found");
        }
    }
}

#endif //THETETRAHEDRON_INIFILE_H

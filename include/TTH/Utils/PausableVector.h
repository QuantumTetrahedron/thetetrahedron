
#ifndef THETETRAHEDRONTEST_PAUSABLEVECTOR_H
#define THETETRAHEDRONTEST_PAUSABLEVECTOR_H

#include <vector>
#include <stack>

template <typename T>
class PausableVector{
public:

    void Pause(){
        pauseStack.push(start);
        start = vec.size();
    }

    void Resume(){
        clear();
        if(!pauseStack.empty()) {
            start = pauseStack.top();
            pauseStack.pop();
        }
    }

    void ClearAll(){
        start = 0;
        while(!pauseStack.empty())
            pauseStack.pop();
        clear();
    }

    void MergeAll(){
        start = 0;
        while(!pauseStack.empty())
            pauseStack.pop();
    }

    T& at(unsigned int pos){
        return vec.at(pos);
    }

    const T& at(unsigned int pos) const{
        return vec.at(pos);
    }

    T& operator[](unsigned int pos){
        return vec[pos];
    }

    const T& operator[](unsigned int pos) const{
        return vec[pos];
    }

    T& front(){
        return vec.front();
    }

    const T& front() const{
        return vec.front();
    }

    T& back(){
        return vec.back();
    }

    const T& back() const{
        return vec.back();
    }

    T* data() noexcept{
        return vec.data();
    }

    const T* data() const noexcept{
        return vec.data();
    }

    auto begin() noexcept {
        return vec.begin()+start;
    }

    const auto begin() const noexcept{
        return vec.begin()+start;
    }

    const auto cbegin() const noexcept{
        return vec.cbegin()+start;
    }

    auto end() noexcept{
        return vec.end();
    }

    const auto end() const noexcept{
        return vec.end();
    }

    const auto cend() const noexcept{
        return vec.cend();
    }

    auto rbegin() noexcept{
        return vec.rbegin();
    }

    const auto rbegin() const noexcept{
        return vec.rbegin();
    }

    const auto crbegin() const noexcept{
        return vec.crbegin();
    }

    auto rend() noexcept{
        return vec.rend();
    }

    const auto rend() const noexcept{
        return vec.rend();
    }

    const auto crend() const noexcept{
        return vec.crend();
    }

    bool empty() const noexcept{
        return start == vec.size();
    }

    unsigned long size() const noexcept{
        return vec.size();
    }

    unsigned long max_size() const noexcept{
        return vec.max_size();
    }

    void reserve(unsigned long new_cap){
        vec.reserve(new_cap);
    }

    unsigned long capacity() const noexcept{
        return vec.capacity();
    }

    void shrink_to_fit(){
        vec.shrink_to_fit();
    }

    void clear() noexcept{
        vec.erase(vec.begin()+start, vec.end());
        //vec.clear();
    }

    auto insert(typename std::vector<T>::const_iterator pos, const T& value){
        return vec.insert(pos, value);
    }

    auto insert(typename std::vector<T>::const_iterator pos, T&& value){
        return vec.insert(pos, value);
    }

    auto insert(typename std::vector<T>::const_iterator pos, unsigned long count, const T& value){
        return vec.insert(pos, count, value);
    }

    template< class InputIt >
    auto insert( typename std::vector<T>::const_iterator pos, InputIt first, InputIt last ){
        return vec.insert(pos, first, last);
    }

    auto insert( typename std::vector<T>::const_iterator pos, std::initializer_list<T> ilist ){
        return vec.insert(pos, ilist);
    }

    auto erase( typename std::vector<T>::const_iterator pos ){
        return vec.erase(pos);
    }

    auto erase( typename std::vector<T>::const_iterator first, typename std::vector<T>::const_iterator last ){
        return vec.erase(first, last);
    }

    void push_back( const T& value ){
        vec.push_back(value);
    }

    void push_back( T&& value ){
        vec.push_back(value);
    }

    void pop_back(){
        vec.pop_back();
    }

    void resize( typename std::vector<T>::size_type count ){
        vec.resize(count);
    }

    void resize( typename std::vector<T>::size_type count, const typename std::vector<T>::value_type& value ){
        vec.resize(count, value);
    }

private:
    std::vector<T> vec;
    std::stack<unsigned long> pauseStack;
    unsigned long start = 0;
};


#endif //THETETRAHEDRONTEST_PAUSABLEVECTOR_H

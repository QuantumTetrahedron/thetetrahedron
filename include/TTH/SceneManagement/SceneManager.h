
#ifndef THETETRAHEDRON_SCENEMANAGER_H
#define THETETRAHEDRON_SCENEMANAGER_H

#include <string>

namespace TTH {
    /**
     * \brief A manager to facilitate transitions between scenes.
     *
     */
    class SceneManager {
    public:
        /**
         * Requests a load of the next scene. At the end of this frame, the current scene will be destroyed and replaced
         * with the new one.
         * @param sceneName - The name of the scene to load next.
         */
        static void LoadNextSceneRequest(std::string sceneName);

        /**
         * Requests a load of another scene without destroying the current one. The current scene will be paused
         * until the ResumeSceneRequest() method is called.
         * @param sceneName - The name of the scene to load.
         */
        static void PauseAndLoadSceneRequest(std::string sceneName);

        /**
         * Requests the destruction of the currently active scenes and unpausing others.
         */
        static void ResumeSceneRequest();

        /**
         * Requests the destruction of every scene paused or not.
         */
        static void ClearAllRequest();

        /**
         * Checks whether a scene change has been requested. Those include pausing and unpausing as well.
         * @return true if the scene is about to change.
         */
        static bool IsSceneChanging();

    private:
        static void Init();
        static void Update(float dt);
        static void ProcessRequest();

        enum class Request{
            Load, PauseAndLoad, Resume, Clear, None
        };
        static Request request;
        static std::string sceneToLoad;

        friend class Game;
        static void SetStartingScene(std::string sceneName);
        static void ClearAll();
        static void LoadNextScene(std::string sceneName);
        static void PauseAndLoadScene(std::string sceneName);
        static void ResumeScene();

        static void LoadObjects(const std::string& sceneName);
    };
}


#endif //THETETRAHEDRON_SCENEMANAGER_H

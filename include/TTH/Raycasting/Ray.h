
#ifndef THETETRAHEDRONTEST_RAY_H
#define THETETRAHEDRONTEST_RAY_H

#include <glm/glm.hpp>
#include "TTH/Utils/PausableVector.h"

namespace TTH {
    class ColliderComponent;

    class Ray {
    public:

        Ray(glm::vec3 pos, glm::vec3 dir);

        ColliderComponent* Hit(const PausableVector<ColliderComponent*>& components, float range);

    private:

        glm::vec3 position, direction;
    };
}


#endif //THETETRAHEDRONTEST_RAY_H

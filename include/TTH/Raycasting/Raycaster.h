
#ifndef THETETRAHEDRONTEST_RAYCASTER_H
#define THETETRAHEDRONTEST_RAYCASTER_H

#include <glm/glm.hpp>
#include "TTH/Utils/PausableVector.h"

namespace TTH {
    class HUDColliderComponent;
    class ColliderComponent;

    class Raycaster {
    public:
        /**
         * Finds a HUDColliderComponent that collides with a given point.
         * @param position - the point to test.
         * @return The HUDColliderComponent that collides with the given point.
         */
        static HUDColliderComponent* HUDRay(glm::vec2 position);

        /**
         * Finds a ColliderComponent that collides with a ray.
         * @param position - The position of the ray origin
         * @param direction - The direction of the ray
         * @return The ColliderComponent that collides with the ray.
         */
        static ColliderComponent* WorldRay(glm::vec3 position, glm::vec3 direction);

    private:
        friend class HUDColliderComponent;
        friend class ColliderComponent;

        static void RegisterHUDElement(HUDColliderComponent* component);
        static void UnregisterHUDElement(HUDColliderComponent* component);

        static void RegisterWorldElement(ColliderComponent* component);
        static void UnregisterWorldElement(ColliderComponent* component);


        static PausableVector<HUDColliderComponent*> HUDElements;
        static PausableVector<ColliderComponent*> worldElements;
    };
}


#endif //THETETRAHEDRONTEST_RAYCASTER_H


#ifndef THETETRAHEDRON_WINDOW_H
#define THETETRAHEDRON_WINDOW_H

#include <string>
#include <GLFW/glfw3.h>
#include <functional>

namespace TTH {

    /**
     * Used internally by the engine.
     */
    class Window {
        friend class Game;
        friend class Input;

        void Generate(int width, int height, const std::string &title);

        void SetKeyCallback(std::function<void(Window, int, int, int, int)> fun);

        void SetCursorPosCallback(std::function<void(Window, double, double)> fun);

        void SetMouseButtonCallback(std::function<void(Window, int,int,int)> fun);

        bool ShouldClose() const;

        void Close();

        void Update();

        void EnableCursor();

        void DisableCursor();

        void SetResolution(int w, int h);
        void SetFullscreen(bool f);

        int width, height;

        std::function<void(Window, int, int, int, int)> keyCallback;
        std::function<void(Window, double, double)> cursorPosCallback;
        std::function<void(Window, int, int, int)> mouseButtonCallback;

    public:
        [[nodiscard]] int getWidth() const;
        [[nodiscard]] int getHeight() const;

        GLFWwindow *window;
    };
}


#endif //THETETRAHEDRON_WINDOW_H


#ifndef THETETRAHEDRON_INPUT_H
#define THETETRAHEDRON_INPUT_H

#include <string>
#include <map>
#include <GLFW/glfw3.h>
#include "Window.h"
#include "MouseHandler.h"

namespace TTH {

    class IniFile;

    /**
     * \brief An input manager.
     *
     * To initialize the input manager, add a .ini file path to the .ini file used for initializing the game.
     * See the GameData/InitData.ini for an example.
     *
     * In the input's .ini file a key-to-axis mapping should be specified.
     * For each user-defined axis, a set of positive and negative keys can be added.
     * See the GameData/Input.ini for an example.
     *
     * \todo Gamepad support
     */
    class Input {
    public:

        /**
         * Gets the value of an axis defined in the .ini file.
         * @param name - name of the axis
         * @return the value of the axis.
         */
        static float GetAxis(const std::string& name);

        /**
         * Checks if a positive button registered to a given axis was pressed in this frame.
         * The key must be released and pressed again to set the value to 1 after this frame passes.
         * @param name - name of the axis.
         * @return 1 if the given axis's button was pressed this frame.
         */
        static int GetButton(const std::string& name);

        /**
         * Gets the mouse position if the cursor is enabled, or the mouse move offset otherwise.
         * @return mouse position or move offset
         */
        static glm::vec2 GetMousePos();
        static glm::vec2 GetMouseOffset();

        /**
         * Disables the cursor by fixing it's position on the center of the screen and making it invisible.
         */
        static void DisableCursor();

        /**
         * Enables the cursor by making it visible and movable.
         */
        static void EnableCursor();

        static void QuitGame();

        static void SetResolution(int w, int h);
        static void SetFullscreen(bool f);
    private:
        friend class Game;

        static void LoadFromFile(IniFile& file);

        static void Initialize(Window* window);

        static void ProcessKeyInput(int key, int state);
        static void ProcessMouseMove(float x, float y);
        static void ProcessMouseButton(int button, int action);
        static void Reset();

        static void RegisterAxis(const std::string& axisName, const std::string& keyName, bool positive);
        static int String2Key(const std::string& name);

        static std::map<int, std::pair<std::string,bool>> key2axis;
        static std::map<std::string, std::pair<float,int>> axes;

        static std::map<std::string, int> string2key;

        static Window* window;
    };
}

#endif //THETETRAHEDRON_INPUT_H

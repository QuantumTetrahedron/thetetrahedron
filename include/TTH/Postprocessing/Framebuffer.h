
#ifndef THETETRAHEDRONTEST_FRAMEBUFFER_H
#define THETETRAHEDRONTEST_FRAMEBUFFER_H

namespace TTH {

    class Texture;

    /**
     * \brief A class representing a framebuffer.
     *
     * This class is used by the engine internally. It is pointless to use it elsewhere.
     */
    class Framebuffer {
    public:
        Framebuffer();
        ~Framebuffer();

        /**
         * Properly generates the framebuffer.
         * @param width - requested framebuffer width
         * @param height - requested framebuffer height
         */
        void Generate(int width, int height, bool hdr = false);

        /**
         * Bind the framebuffer.
         */
        void BindAsTarget();

        /**
         * Unbinds the framebuffer
         */
        void Unbind();

        /**
         * Renders the framebuffer
         */
        void Display() const;

        /**
         * Retrieves the texture that the framebuffer was drawing to
         * @return the framebuffer's texture.
         */
        Texture* getTexture();
    private:
        unsigned int fbo;

        Texture* texture;

        class Renderer{
        public:
            void Init();
            void Draw(const Framebuffer* framebuffer);
            bool isInitialized() const;
        private:
            bool initialized = false;
            unsigned int VAO, VBO;
        };
        static Renderer renderer;
        int h, w;
    };
}


#endif //THETETRAHEDRONTEST_FRAMEBUFFER_H


#ifndef THETETRAHEDRONTEST_POSTPROCESSING_H
#define THETETRAHEDRONTEST_POSTPROCESSING_H

#include <string>
#include <map>
#include "Framebuffer.h"

namespace TTH {

    class Shader;

    /**
     * The postprocessing manager. It is used internaly by the engine.
     *
     * It is not supported to apply postprocessing effects on normal textures.
     * Any postprocessing effect will only work on framebuffers.
     *
     * It is advised to not use this class explicitly.
     */
    class Postprocessing {
    public:
        /**
         * This function executes the effect on a given framebuffer. To create a custom postprocessing effect
         * simply create a shader and pass the file paths to the main .ini file
         *
         * The following data will be available to use in the shaders (apart from vertex position and texture coordinates):
         * - uniform sampler2D screenTexture; - The texture to process.
         * - uniform int screenWidth; - The width of the framebuffer in pixels.
         * - uniform int screenHeight; - The height of the framebuffer in pixels.
         *
         * @param framebuffer - The framebuffer to process.
         * @param effectName - The name of the effect to apply.
         */
        static void Process(Framebuffer& framebuffer, std::string effectName);
        static void ProcessTo(Framebuffer& from, Framebuffer& to, const std::string& effectName);
    private:
        static Framebuffer offScreen;
        static Shader* offScreenShader;
        static int screenWidth, screenHeight;

        friend class Game;
        static void Init();
    };
}

#endif //THETETRAHEDRONTEST_POSTPROCESSING_H

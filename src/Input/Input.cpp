
#include "TTH/Input/Input.h"
#include "Input_Out.h"
#include <GLFW/glfw3.h>

namespace TTH{
    std::map<int, std::pair<std::string, bool>> Input::key2axis;
    std::map<std::string, std::pair<float,int>> Input::axes;
    std::map<std::string, int> Input::string2key;
    Window* Input::window;

    float Input::GetAxis(const std::string &name) {
        return axes.at(name).first;
    }

    void Input::ProcessKeyInput(int key, int action) {
        auto it = key2axis.find(key);
        if(it == key2axis.end()) return;

        std::pair<std::string, bool> p = it->second;

        auto axis = axes.find(p.first);
        if(axis != axes.end()){
            if(action == GLFW_PRESS) {
                if (p.second) {
                    axis->second.first += 1;
                    axis->second.second = 1;
                } else {
                    axis->second.first -= 1;
                }
            }else if(action == GLFW_RELEASE){
                if(p.second){
                    axis->second.first -= 1;
                } else {
                    axis->second.first += 1;
                }
            }
        }
    }

    void Input::ProcessMouseButton(int button, int action) {
        MouseHandler::ProcessButton(button, action);
    }

    void Input::LoadFromFile(IniFile& file) {
        file.SetToSection("");
        bool disableMouse;
        file.GetValue("mouseDisabled", disableMouse, false);
        if(disableMouse)
            DisableCursor();

        std::string mouseButtonMode;
        if(!file.GetValue("mouseButtonMode", mouseButtonMode)){
            mouseButtonMode = "none";
        }
        MouseHandler::Initialize(mouseButtonMode);

        std::string axesList;
        if(!file.GetValue("axes", axesList)){
            axesList = "";
        }
        std::stringstream ss(axesList);
        std::string axis;
        while(ss >> axis){
            file.SetToSection(axis);
            std::string keyList;

            file.GetValue("positive", keyList);
            std::stringstream pos_ss(keyList);
            std::string keyName;
            while(pos_ss >> keyName){
                RegisterAxis(axis, keyName, true);
            }

            keyList = "";
            file.GetValue("negative", keyList);
            std::stringstream neg_ss(keyList);
            while(neg_ss >> keyName){
                RegisterAxis(axis, keyName, false);
            }
        }
    }

    void Input::RegisterAxis(const std::string &axisName, const std::string &keyName, bool positive) {
        int key = String2Key(keyName);
        auto k2a_iter = key2axis.find(key);
        if(k2a_iter == key2axis.end())
            key2axis.emplace(std::make_pair(key, std::make_pair(axisName, positive)));
        else
            return;

        auto axes_iter = axes.find(axisName);
        if(axes_iter == axes.end()){
            axes.emplace(std::make_pair(axisName, std::make_pair(0.0f,0)));
        }
    }

    int Input::String2Key(const std::string &name) {
        return string2key.at(name);
    }

    void Input::Initialize(Window* win) {
        window = win;

        string2key.emplace(std::make_pair("0", GLFW_KEY_0));
        string2key.emplace(std::make_pair("1", GLFW_KEY_1));
        string2key.emplace(std::make_pair("2", GLFW_KEY_2));
        string2key.emplace(std::make_pair("3", GLFW_KEY_3));
        string2key.emplace(std::make_pair("4", GLFW_KEY_4));
        string2key.emplace(std::make_pair("5", GLFW_KEY_5));
        string2key.emplace(std::make_pair("6", GLFW_KEY_6));
        string2key.emplace(std::make_pair("7", GLFW_KEY_7));
        string2key.emplace(std::make_pair("8", GLFW_KEY_8));
        string2key.emplace(std::make_pair("9", GLFW_KEY_9));
        string2key.emplace(std::make_pair("A", GLFW_KEY_A));
        string2key.emplace(std::make_pair("Apostrophe", GLFW_KEY_APOSTROPHE));
        string2key.emplace(std::make_pair("B", GLFW_KEY_B));
        string2key.emplace(std::make_pair("Backslash", GLFW_KEY_BACKSLASH));
        string2key.emplace(std::make_pair("Backspace", GLFW_KEY_BACKSPACE));
        string2key.emplace(std::make_pair("C", GLFW_KEY_C));
        string2key.emplace(std::make_pair("CapsLock", GLFW_KEY_CAPS_LOCK));
        string2key.emplace(std::make_pair("Comma", GLFW_KEY_COMMA));
        string2key.emplace(std::make_pair("D", GLFW_KEY_D));
        string2key.emplace(std::make_pair("Delete", GLFW_KEY_DELETE));
        string2key.emplace(std::make_pair("Down", GLFW_KEY_DOWN));
        string2key.emplace(std::make_pair("E", GLFW_KEY_E));
        string2key.emplace(std::make_pair("Esc", GLFW_KEY_ESCAPE));
        string2key.emplace(std::make_pair("End", GLFW_KEY_END));
        string2key.emplace(std::make_pair("Enter", GLFW_KEY_ENTER));
        string2key.emplace(std::make_pair("Equal", GLFW_KEY_EQUAL));
        string2key.emplace(std::make_pair("F", GLFW_KEY_F));
        string2key.emplace(std::make_pair("F1", GLFW_KEY_F1));
        string2key.emplace(std::make_pair("F2", GLFW_KEY_F2));
        string2key.emplace(std::make_pair("F3", GLFW_KEY_F3));
        string2key.emplace(std::make_pair("F4", GLFW_KEY_F4));
        string2key.emplace(std::make_pair("F5", GLFW_KEY_F5));
        string2key.emplace(std::make_pair("F6", GLFW_KEY_F6));
        string2key.emplace(std::make_pair("F7", GLFW_KEY_F7));
        string2key.emplace(std::make_pair("F8", GLFW_KEY_F8));
        string2key.emplace(std::make_pair("F9", GLFW_KEY_F9));
        string2key.emplace(std::make_pair("F10", GLFW_KEY_F10));
        string2key.emplace(std::make_pair("F11", GLFW_KEY_F11));
        string2key.emplace(std::make_pair("F12", GLFW_KEY_F12));
        string2key.emplace(std::make_pair("F13", GLFW_KEY_F13));
        string2key.emplace(std::make_pair("F14", GLFW_KEY_F14));
        string2key.emplace(std::make_pair("F15", GLFW_KEY_F15));
        string2key.emplace(std::make_pair("F16", GLFW_KEY_F16));
        string2key.emplace(std::make_pair("F17", GLFW_KEY_F17));
        string2key.emplace(std::make_pair("F18", GLFW_KEY_F18));
        string2key.emplace(std::make_pair("F19", GLFW_KEY_F19));
        string2key.emplace(std::make_pair("F20", GLFW_KEY_F20));
        string2key.emplace(std::make_pair("F21", GLFW_KEY_F21));
        string2key.emplace(std::make_pair("F22", GLFW_KEY_F22));
        string2key.emplace(std::make_pair("F23", GLFW_KEY_F23));
        string2key.emplace(std::make_pair("F24", GLFW_KEY_F24));
        string2key.emplace(std::make_pair("F25", GLFW_KEY_F25));
        string2key.emplace(std::make_pair("G", GLFW_KEY_G));
        string2key.emplace(std::make_pair("GraveAccent", GLFW_KEY_GRAVE_ACCENT));
        string2key.emplace(std::make_pair("H", GLFW_KEY_H));
        string2key.emplace(std::make_pair("Home", GLFW_KEY_HOME));
        string2key.emplace(std::make_pair("I", GLFW_KEY_I));
        string2key.emplace(std::make_pair("Insert", GLFW_KEY_INSERT));
        string2key.emplace(std::make_pair("J", GLFW_KEY_J));
        string2key.emplace(std::make_pair("K", GLFW_KEY_K));
        string2key.emplace(std::make_pair("KP0", GLFW_KEY_KP_0));
        string2key.emplace(std::make_pair("KP1", GLFW_KEY_KP_1));
        string2key.emplace(std::make_pair("KP2", GLFW_KEY_KP_2));
        string2key.emplace(std::make_pair("KP3", GLFW_KEY_KP_3));
        string2key.emplace(std::make_pair("KP4", GLFW_KEY_KP_4));
        string2key.emplace(std::make_pair("KP5", GLFW_KEY_KP_5));
        string2key.emplace(std::make_pair("KP6", GLFW_KEY_KP_6));
        string2key.emplace(std::make_pair("KP7", GLFW_KEY_KP_7));
        string2key.emplace(std::make_pair("KP8", GLFW_KEY_KP_8));
        string2key.emplace(std::make_pair("KP9", GLFW_KEY_KP_9));
        string2key.emplace(std::make_pair("KPAdd", GLFW_KEY_KP_ADD));
        string2key.emplace(std::make_pair("KPDecimal", GLFW_KEY_KP_DECIMAL));
        string2key.emplace(std::make_pair("KPDivide", GLFW_KEY_KP_DIVIDE));
        string2key.emplace(std::make_pair("KPEnter", GLFW_KEY_KP_ENTER));
        string2key.emplace(std::make_pair("KPEqual", GLFW_KEY_KP_EQUAL));
        string2key.emplace(std::make_pair("KPMultiply", GLFW_KEY_KP_MULTIPLY));
        string2key.emplace(std::make_pair("KPSubtract", GLFW_KEY_KP_SUBTRACT));
        string2key.emplace(std::make_pair("L", GLFW_KEY_L));
        string2key.emplace(std::make_pair("Last", GLFW_KEY_LAST));
        string2key.emplace(std::make_pair("Left", GLFW_KEY_LEFT));
        string2key.emplace(std::make_pair("LeftAlt", GLFW_KEY_LEFT_ALT));
        string2key.emplace(std::make_pair("LeftBracket", GLFW_KEY_LEFT_BRACKET));
        string2key.emplace(std::make_pair("LeftControl", GLFW_KEY_LEFT_CONTROL));
        string2key.emplace(std::make_pair("LeftShit", GLFW_KEY_LEFT_SHIFT));
        string2key.emplace(std::make_pair("LeftSuper", GLFW_KEY_LEFT_SUPER));
        string2key.emplace(std::make_pair("M", GLFW_KEY_M));
        string2key.emplace(std::make_pair("Menu", GLFW_KEY_MENU));
        string2key.emplace(std::make_pair("Minus", GLFW_KEY_MINUS));
        string2key.emplace(std::make_pair("N", GLFW_KEY_N));
        string2key.emplace(std::make_pair("NumLock", GLFW_KEY_NUM_LOCK));
        string2key.emplace(std::make_pair("O", GLFW_KEY_O));
        string2key.emplace(std::make_pair("P", GLFW_KEY_P));
        string2key.emplace(std::make_pair("PageDown", GLFW_KEY_PAGE_DOWN));
        string2key.emplace(std::make_pair("PageUp", GLFW_KEY_PAGE_UP));
        string2key.emplace(std::make_pair("Pause", GLFW_KEY_PAUSE));
        string2key.emplace(std::make_pair("Period", GLFW_KEY_PERIOD));
        string2key.emplace(std::make_pair("PrintScreen", GLFW_KEY_PRINT_SCREEN));
        string2key.emplace(std::make_pair("Q", GLFW_KEY_Q));
        string2key.emplace(std::make_pair("R", GLFW_KEY_R));
        string2key.emplace(std::make_pair("Right", GLFW_KEY_RIGHT));
        string2key.emplace(std::make_pair("RightAlt", GLFW_KEY_RIGHT_ALT));
        string2key.emplace(std::make_pair("RightBracket", GLFW_KEY_RIGHT_BRACKET));
        string2key.emplace(std::make_pair("RightControl", GLFW_KEY_RIGHT_CONTROL));
        string2key.emplace(std::make_pair("RightShift", GLFW_KEY_RIGHT_SHIFT));
        string2key.emplace(std::make_pair("RightSuper", GLFW_KEY_RIGHT_SUPER));
        string2key.emplace(std::make_pair("S", GLFW_KEY_S));
        string2key.emplace(std::make_pair("ScrollLock", GLFW_KEY_SCROLL_LOCK));
        string2key.emplace(std::make_pair("Semicolon", GLFW_KEY_SEMICOLON));
        string2key.emplace(std::make_pair("Slash", GLFW_KEY_SLASH));
        string2key.emplace(std::make_pair("Space", GLFW_KEY_SPACE));
        string2key.emplace(std::make_pair("T", GLFW_KEY_T));
        string2key.emplace(std::make_pair("Tab", GLFW_KEY_TAB));
        string2key.emplace(std::make_pair("U", GLFW_KEY_U));
        string2key.emplace(std::make_pair("Up", GLFW_KEY_UP));
        string2key.emplace(std::make_pair("V", GLFW_KEY_V));
        string2key.emplace(std::make_pair("W", GLFW_KEY_W));
        string2key.emplace(std::make_pair("World1", GLFW_KEY_WORLD_1));
        string2key.emplace(std::make_pair("World2", GLFW_KEY_WORLD_2));
        string2key.emplace(std::make_pair("X", GLFW_KEY_X));
        string2key.emplace(std::make_pair("Y", GLFW_KEY_Y));
        string2key.emplace(std::make_pair("Z", GLFW_KEY_Z));
        
    }

    int Input::GetButton(const std::string &name) {
        return axes.at(name).second;
    }

    void Input::Reset() {
        for(auto& axis : axes){
            axis.second.second = 0;
        }
        MouseHandler::Reset();
    }

    glm::vec2 Input::GetMousePos() {
        return MouseHandler::GetMousePos();
    }

    glm::vec2 Input::GetMouseOffset() {
        return MouseHandler::GetMouseOffset();
    }

    void Input::ProcessMouseMove(float x, float y) {
        MouseHandler::ProcessMouseMove(x,y);
    }

    void Input::DisableCursor() {
        MouseHandler::DisableCursor();
        window->DisableCursor();
    }

    void Input::EnableCursor() {
        MouseHandler::EnableCursor();
        window->EnableCursor();
    }

    void Input::QuitGame() {
        window->Close();
    }


    void Input::SetResolution(int w, int h) {
        window->SetResolution(w,h);
    }

    void Input::SetFullscreen(bool f) {
        window->SetFullscreen(f);
    }
}
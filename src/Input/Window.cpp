
#include <iostream>
#include <glad/glad.h>
#include <functional>
#include <TTH/Input/Window.h>

#include "TTH/Input/Window.h"
#include "Input_Out.h"

namespace TTH {
    void Window::Generate(int width, int height, const std::string &title) {
        window = glfwCreateWindow(width, height, title.c_str(), nullptr, nullptr);

        this->width = width;
        this->height = height;

        if (!window) {
            std::cerr << "Failed to create GLFW window" << std::endl;
            glfwTerminate();
            throw std::runtime_error("Failed to create GLFW window");
        }
        glfwMakeContextCurrent(window);

        if (!gladLoadGLLoader((GLADloadproc) glfwGetProcAddress)) {
            std::cout << "Failed to initialize GLAD" << std::endl;
            throw std::runtime_error("Failed to initialize GLAD");
        }
    }

    void Window::SetKeyCallback(std::function<void(Window, int, int, int, int)> fun) {
        keyCallback = fun;
        glfwSetWindowUserPointer(window, (void *) this);
        glfwSetKeyCallback(window, [](GLFWwindow *win, int key, int scancode, int action, int mods) {
            auto t = (Window *) glfwGetWindowUserPointer(win);
            t->keyCallback(*t, key, scancode, action, mods);
        });
    }

    void Window::SetCursorPosCallback(std::function<void(Window, double, double)> fun) {
        cursorPosCallback = fun;
        glfwSetWindowUserPointer(window, (void *) this);
        glfwSetCursorPosCallback(window, [](GLFWwindow *win, double xPos, double yPos) {
            auto t = (Window *) glfwGetWindowUserPointer(win);
            t->cursorPosCallback(*t, xPos, yPos);
        });
    }

    bool Window::ShouldClose() const {
        return glfwWindowShouldClose(window) != 0;
    }

    void Window::Update() {
        glfwSwapBuffers(window);
        glfwPollEvents();
    }

    void Window::EnableCursor() {
        glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
    }

    void Window::DisableCursor() {
        glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
    }

    void Window::SetMouseButtonCallback(std::function<void(Window, int, int, int)> fun) {
        mouseButtonCallback = fun;
        glfwSetWindowUserPointer(window, (void*) this);
        glfwSetMouseButtonCallback(window, [](GLFWwindow* win, int button, int action, int mods){
            auto t = (Window*)glfwGetWindowUserPointer(win);
            t->mouseButtonCallback(*t, button, action, mods);
        });
    }

    void Window::Close() {
        glfwSetWindowShouldClose(window, true);
    }

    void Window::SetResolution(int w, int h) {
        glfwSetWindowSize(window, w, h);
        width = w;
        height = h;
        Gfx::SetResolution(w,h);
        glViewport(0,0,w,h);
    }

    void Window::SetFullscreen(bool f) {
        const GLFWvidmode* mode = glfwGetVideoMode(glfwGetPrimaryMonitor());
        if(f){
            glfwSetWindowMonitor(window, glfwGetPrimaryMonitor(),0,0,width,height,mode->refreshRate);
        } else {
            glfwSetWindowMonitor(window, nullptr, 0, 0, width, height, mode->refreshRate);
        }
    }

    int Window::getWidth() const {
        return width;
    }

    int Window::getHeight() const {
        return height;
    }
}
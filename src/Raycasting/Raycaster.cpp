
#include <algorithm>
#include "TTH/Raycasting/Raycaster.h"
#include "Raycasting_Out.h"
#include "TTH/Raycasting/Ray.h"

namespace TTH{
    PausableVector<HUDColliderComponent*> Raycaster::HUDElements;
    PausableVector<ColliderComponent*> Raycaster::worldElements;

    HUDColliderComponent *Raycaster::HUDRay(glm::vec2 position) {
        for(HUDColliderComponent* b : HUDElements){
            if(b->Collides(position)){
                return b;
            }
        }
        return nullptr;
    }

    void Raycaster::RegisterHUDElement(HUDColliderComponent *component) {
        HUDElements.push_back(component);
    }

    void Raycaster::UnregisterHUDElement(HUDColliderComponent *component) {
        HUDElements.erase(std::remove_if(HUDElements.begin(), HUDElements.end(), [component](auto c){return c==component;}),HUDElements.end());
    }

    ColliderComponent *Raycaster::WorldRay(glm::vec3 position, glm::vec3 direction) {
        Ray ray(position, direction);
        return ray.Hit(worldElements, 100.0f);
    }

    void Raycaster::RegisterWorldElement(ColliderComponent *component) {
        worldElements.push_back(component);
        //std::cout << "Elements: " << worldElements.size() << std::endl;
    }

    void Raycaster::UnregisterWorldElement(ColliderComponent *component) {
        worldElements.erase(std::remove_if(worldElements.begin(), worldElements.end(), [component](auto c){return c == component;}),worldElements.end());
        //std::cout << "Elements: " << worldElements.size() << std::endl;
    }
}
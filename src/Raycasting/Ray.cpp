
#include "TTH/Raycasting/Ray.h"
#include "Raycasting_Out.h"

namespace TTH{

    Ray::Ray(glm::vec3 pos, glm::vec3 dir) {
        position = pos;
        direction = dir;
    }

    ColliderComponent *Ray::Hit(const PausableVector<ColliderComponent *> &components, float range) {
        glm::mat4 lookAt = glm::lookAt(position, position+direction, glm::vec3(0.0f,1.0f,0.0f));

        ColliderComponent* nearest = nullptr;
        float nearestZ = -range;
        for(ColliderComponent* c : components){
            float Z = (lookAt*glm::vec4(c->parent->transform.position,1.0f)).z;
            if(Z < nearestZ || Z > 0.0f) continue;

            glm::vec3 scale = c->GetScale() / c->parent->transform.scale;
            glm::vec3 p = c->parent->transform.position;
            glm::vec3 vertices[8] = {
                    glm::vec3(-scale.x, -scale.y, -scale.z),
                    glm::vec3(-scale.x, -scale.y,  scale.z),
                    glm::vec3(-scale.x,  scale.y, -scale.z),
                    glm::vec3(-scale.x,  scale.y,  scale.z),
                    glm::vec3( scale.x, -scale.y, -scale.z),
                    glm::vec3( scale.x, -scale.y,  scale.z),
                    glm::vec3( scale.x,  scale.y, -scale.z),
                    glm::vec3( scale.x,  scale.y,  scale.z)
            };
            for(int i=0;i<8;i++){
                vertices[i] = glm::vec3(lookAt * c->parent->transform.GetModelMatrix() * glm::vec4(vertices[i],1.0f));
            }

            // For each edge we check if the point is on the other side. If this is false for every edge, we hit
            bool outOfArea = false;
            for(int i=0;i<8;i++){
                for(int j=0;j<8;j++){
                    if(i==j) continue;
                    glm::vec2 A = glm::vec2(vertices[i]);
                    glm::vec2 B = glm::vec2(vertices[j]);

                    bool otherPointsOnOneSide = true;
                    float lastDet = 0.0f;
                    for(int k=0;k<8;k++){
                        if(k == i || k == j) continue;
                        glm::vec2 C = glm::vec2(vertices[k]);
                        float det = A.x*B.y-A.y*B.x-A.x*C.y+A.y*C.x+B.x*C.y-B.y*C.x;
                        if ((lastDet > 0.0f && det < 0.0f)||(lastDet < 0.0f && det > 0.0f)) {
                            otherPointsOnOneSide = false;
                            break;
                        }
                        lastDet = det;
                    }
                    if(otherPointsOnOneSide){
                        float rayDet = A.x*B.y-A.y*B.x;
                        if ((lastDet > 0.0f && rayDet < 0.0f)||(lastDet < 0.0f && rayDet > 0.0f)) {
                            //point out of area. No need to check other area edges.
                            outOfArea = true;
                            break;
                        }
                    }
                }
                if(outOfArea) break;
            }
            if(!outOfArea){
                nearest = c;
                nearestZ = Z;
            }
        }
        return nearest;
    }
}

#include <glm/gtc/matrix_transform.hpp>
#include "TTH/Core/Transform.h"

namespace TTH {

    glm::mat4 Transform::GetModelMatrix() const {
        glm::mat4 model(1.0f);

        glm::vec3 offset = glm::vec3(scale.x*pivot.x, scale.y*pivot.y, scale.z*pivot.z);
        model = glm::translate(model, position-offset);

        if (glm::length(rotation) > 0) {
            model = glm::translate(model, offset);
            model = glm::rotate(model, glm::radians(rotation.y), glm::vec3(0.0f, 1.0f, 0.0f));
            model = glm::rotate(model, glm::radians(rotation.z), glm::vec3(0.0f, 0.0f, 1.0f));
            model = glm::rotate(model, glm::radians(rotation.x), glm::vec3(1.0f, 0.0f, 0.0f));
            model = glm::translate(model, -offset);
        }
        model = glm::scale(model, scale);

        return model;
    }
}
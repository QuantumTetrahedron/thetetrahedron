
#include "TTH/Core/ObjectFactory.h"
#include "Core_Out.h"

namespace TTH {
    IniFile ObjectFactory::currArchetype;
    bool ObjectFactory::useArchetype = false;

    Object* ObjectFactory::Create(IniFile& objectFile, const std::string& objectName) {
        auto newObject = new Object();

        newObject->name = objectName;

        IniFile file;
        if(useArchetype){
            file = currArchetype.UpdateWith(objectFile);
        } else {
            file = objectFile;
        }

        newObject->LoadFromFile(file);

        std::string components;
        file.GetValue("components", components);
        std::stringstream ss(components);

        std::string compName;
        while (ss >> compName){
            Component* comp = ComponentFactory::create(compName);
            if(!comp->LoadFromFile(file)){
                return nullptr;
            }
            newObject->AddComponent(comp);
        }

        for(Component* c : newObject->GetAllComponents<Component>()){
            c->OnLoad();
        }

        return newObject;
    }

    void ObjectFactory::SetArchetype(IniFile archetypeFile) {
        currArchetype = archetypeFile;
        useArchetype = true;
    }

    void ObjectFactory::RemoveArchetype() {
        useArchetype = false;
    }
}
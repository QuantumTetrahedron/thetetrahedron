
#include "TTH/Core/Object.h"
#include "TTH/Core/BehaviourComponent.h"
#include "Core_Out.h"
#include <queue>
#include <algorithm>
#include <iostream>
#include <glm/gtc/matrix_transform.hpp>

namespace TTH{

    Object::Object()
    : isDead(false), isActive(true), parent(nullptr){
        transform.position = glm::vec3(0.0f);
        transform.rotation = glm::vec3(0.0f);
        transform.scale = glm::vec3(1.0f);
        transform.pivot = glm::vec3(0.0f);
    }

    Object::~Object() {
        RemoveAllComponents();
    }

    void Object::Update(float dt) {
        for(BehaviourComponent* bc : behaviours){
            bc->Update(dt);
        }

        std::queue<Component*> toDelete;
        for(Component* c : components)
        {
            if(c->isDead)
                toDelete.push(c);
        }
        while(!toDelete.empty()){
            Component* c = toDelete.front();
            toDelete.pop();
            RemoveComponent(c);
        }
    }

    void Object::AddComponent(Component *c) {
        if(std::find(components.begin(), components.end(), c) != components.end()){
            std::cerr << "Component already added" << std::endl;
            return;
        }
        c->SetParent(this);
        components.push_back(c);

        auto bc = dynamic_cast<BehaviourComponent*>(c);
        if(bc){
            behaviours.push_back(bc);
        }
    }

    void Object::RemoveComponent(Component *c) {
        auto i = std::find(components.begin(), components.end(), c);
        if(i == components.end()){
            std::cerr << "Component not found" << std::endl;
            return;
        }
        std::iter_swap(i, --components.end());
        components.pop_back();

        auto bc = dynamic_cast<BehaviourComponent*>(c);
        if(bc){
            auto bi = std::find(behaviours.begin(),behaviours.end(),bc);
            if(bi!=behaviours.end()){
                std::iter_swap(bi, --behaviours.end());
                behaviours.pop_back();
            }
        }

        c->SetParent(nullptr);
        delete c;
    }

    void Object::RemoveAllComponents() {
        for(Component* c : components){
            c->SetParent(nullptr);
            delete c;
            c = nullptr;
        }
        components.clear();
        behaviours.clear();
    }

    void Object::LoadFromFile(IniFile& file) {
        file.SetToSection("");
        file.GetValue("position", transform.position, glm::vec3(0.0f));
        file.GetValue("rotation", transform.rotation, glm::vec3(0.0f));
        file.GetValue("scale", transform.scale, glm::vec3(1.0f));
        file.GetValue("pivot", transform.pivot, glm::vec3(0.0f));
    }

    Object *Object::Clone() const {
        auto ret = new Object();
        ret->components.clear();
        for(Component* c : components){
            ret->AddComponent(c->Clone());
        }
        ret->transform = transform;
        return ret;
    }

    void Object::SetParent(Object *o) {
        parent = o;
    }

    Transform Object::GetWorldTransform() const {
        if(parent) {
            Transform ret;
            Transform parentTransform = parent->GetWorldTransform();
            glm::mat4 model(1.0f);
            model = glm::rotate(model, glm::radians(parentTransform.rotation.y), glm::vec3(0.0f, 1.0f, 0.0f));
            model = glm::rotate(model, glm::radians(parentTransform.rotation.z), glm::vec3(0.0f, 0.0f, 1.0f));
            model = glm::rotate(model, glm::radians(parentTransform.rotation.x), glm::vec3(1.0f, 0.0f, 0.0f));
            model = glm::scale(model, glm::vec3(parentTransform.scale));
            ret.position = glm::vec3(model * glm::vec4(transform.position - parentTransform.pivot, 1.0f)) + (parentTransform.position);
            ret.rotation = parentTransform.rotation + transform.rotation;
            ret.scale = parentTransform.scale * transform.scale;
            ret.pivot = transform.pivot; // Pivot is always local
            return ret;
        }
        else return transform;
    }

    bool Object::HasParent() const {
        return parent != nullptr;
    }

    bool Object::IsDead() const {
        return isDead;
    }

    void Object::OnDestroy() {
        for(BehaviourComponent* bc : behaviours)
            bc->OnLeave();
    }

    void Object::Start() {
        for(BehaviourComponent* bc : behaviours)
            bc->Start();
    }

    bool Object::IsActive() const{
        if(parent)
            return isActive && parent->IsActive();
        else
            return isActive;
    }

    void Object::SetActive(bool active) {
        bool wasActive = isActive;
        isActive = active;
        if(!wasActive && IsActive()){
            OnActive();
        }
    }

    void Object::OnActive() {
        for(Component* c : components){
            if(c->IsActive()){
                c->OnActive();
            }
        }
    }

    std::string Object::GetName() const {
        return name;
    }

    void Object::Kill() {
        isDead = true;
    }
}
#include "TTH/Core/ComponentFactory.h"
#include <iostream>

namespace TTH{
    ComponentFactory::mapType* ComponentFactory::map;

    Component *ComponentFactory::create(const std::string &className){
        auto it = getMap()->find(className);
        if(it == getMap()->end()){
            std::cerr << "Component type " << className << " not registered" << std::endl;
            return nullptr;
        }
        else{
            return it->second();
        }
    }

    ComponentFactory::mapType *ComponentFactory::getMap() {
        if(!map){
            map = new mapType;
        }
        return map;
    }
}

#include "TTH/Core/Component.h"

namespace TTH {
    Component::Component() : isDead(false), isActive(true){}

    void Component::SetParent(Object *p) {
        parent = p;
    }

    const Object *Component::GetParent() const {
        return parent;
    }

    bool Component::IsDead() const {
        return isDead;
    }

    void Component::Kill() {
        isDead = true;
    }

    bool Component::IsActive() const {
        if(parent)
            return isActive && parent->IsActive();
        else
            return isActive;
    }

    void Component::SetActive(bool active) {
        bool wasActive = isActive;
        isActive = active;
        if(!wasActive && IsActive()){
            OnActive();
        }
    }
}


#include "TTH/Core/CameraComponent.h"
#include "Core_Out.h"
#include <glm/gtc/matrix_transform.hpp>

namespace TTH{
    ComponentRegister<CameraComponent> CameraComponent::reg("CameraComponent");

    CameraComponent::CameraComponent()
    : worldUp(0.0f,1.0f,0.0f) {
        fieldOfView = 0.0f;
        aspectRatio = 0.0f;
    }

    glm::mat4 CameraComponent::GetViewMatrix() {
        glm::vec3 position = parent->transform.position;
        return glm::lookAt(position, position+front, worldUp);
    }

    glm::mat4 CameraComponent::GetProjectionMatrix() {
        return glm::perspective(fieldOfView, aspectRatio, 0.1f, 100.0f);
    }

    bool CameraComponent::LoadFromFile(IniFile& file) {
        file.SetToSection("CameraComponent");

        file.RequireValue("fieldOfView", fieldOfView);

        glm::vec2 resolution;
        if(file.GetValue("resolution", resolution)){
            screenWidth = (int)resolution.x;
            screenHeight = (int)resolution.y;
        } else {
            Gfx::GetResolution(&screenWidth, &screenHeight);
        }

        aspectRatio = (float)screenWidth/screenHeight;

        file.GetValue("backgroundColor", backgroundColor, glm::vec3(0.0f));

        file.GetValue("isMain", isMain, false);

        file.GetValue("drawWorldSpace", drawWorldSpace, true);
        file.GetValue("drawHUDSpace", drawHUDSpace, true);

        framebuffer = new Framebuffer();
        framebuffer->Generate(screenWidth, screenHeight);

        return true;
    }

    CameraComponent *CameraComponent::Clone() const {
        auto cam = new CameraComponent();
        cam->fieldOfView = fieldOfView;
        cam->aspectRatio = aspectRatio;
        return cam;
    }

    glm::vec3 CameraComponent::GetPosition() {
        return parent->transform.position;
    }

    int CameraComponent::GetWidth() const {
        return screenWidth;
    }

    int CameraComponent::GetHeight() const {
        return screenHeight;
    }

    glm::vec3 CameraComponent::GetFront() const {
        return front;
    }

    void CameraComponent::Use() {
        //framebuffer->BindAsTarget();
        Gfx::UseFramebuffer(framebuffer);
        glClearColor(backgroundColor.x, backgroundColor.y, backgroundColor.z, 1.0f);
    }

    glm::mat4 CameraComponent::GetHUDProjectionMatrix() {
        return glm::ortho(0.0f, (float)screenWidth, (float)screenHeight, 0.0f, -100.0f, 100.0f);
    }

    CameraComponent::~CameraComponent() {
        Gfx::UnregisterCamera(this);
        delete framebuffer;
    }

    void CameraComponent::OnLoad() {
        Gfx::RegisterCamera(this);
    }

    void CameraComponent::EndUse() {
        //framebuffer->Unbind();
        Gfx::UseNoFramebuffer();
        lastFrameTexture = framebuffer->getTexture();

        // Applies all the postprocessing when the camera is done rendering
        for(const std::string& name : postprocessEffects){
            Postprocessing::Process(*framebuffer, name);
        }
    }

    Texture* CameraComponent::GetLastFrameTexture() {
        return lastFrameTexture;
    }

    void CameraComponent::Display() {
        Gfx::DisplayFramebuffer(*framebuffer);
    }

    void CameraComponent::AddPostprocessEffect(const std::string &name) {
        postprocessEffects.push_back(name);
    }

    void CameraComponent::RemovePostprocessEffect(const std::string &name) {
        for(auto i = postprocessEffects.begin(); i!=postprocessEffects.end(); ++i){
            if(*i == name){
                postprocessEffects.erase(i);
                break;
            }
        }
    }

    void CameraComponent::SetResolution(int w, int h) {
        screenWidth = w;
        screenHeight = h;

        delete framebuffer;
        framebuffer = new Framebuffer();
        framebuffer->Generate(screenWidth, screenHeight);
    }

    void CameraComponent::SetFront(glm::vec3 newFront) {
        front = newFront;
    }
}
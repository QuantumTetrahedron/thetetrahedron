
#include "TTH/Core/Game.h"
#include "Core_Out.h"
#include "TTH/Utils/IniFile.h"
#include <iostream>

namespace TTH {

    Window Game::window;
    float Game::deltaTime;
    float Game::lastFrameTime;
    int Game::fps;
    int Game::lastFPS;

    void Game::Initialize(const char *configFilePath) {
        Gfx::Init();

        IniFile file;
        file.ReadFile(configFilePath);

        std::string gameOptionsPath;
        std::string renderingOptionsPath;
        std::string resourceOptionsPath;
        std::string inputOptionsPath;

        file.SetToSection("OptionsFilePaths");
        file.RequireValue("game", gameOptionsPath);
        file.RequireValue("rendering", renderingOptionsPath);
        file.RequireValue("resources", resourceOptionsPath);
        file.RequireValue("input", inputOptionsPath);

        LoadWindow(gameOptionsPath);
        LoadRenderingOptions(renderingOptionsPath);
        LoadResources(resourceOptionsPath);
        LoadInput(inputOptionsPath);
        LoadGame(gameOptionsPath);
    }

    void Game::MainLoop() {
        static float time = 0.0f;
        while (!window.ShouldClose()) {
            auto currentFrameTime = (float)glfwGetTime();
            deltaTime = currentFrameTime - lastFrameTime;
            lastFrameTime = currentFrameTime;
            time += deltaTime;
            fps++;
            if(time >= 1.0f){
                time -= 1.0f;
                lastFPS = fps;
                fps = 0;
            }

            // Updates every active scene
            SceneManager::Update(deltaTime);

            Input::Reset();

            Gfx::Draw();

            SceneManager::ProcessRequest();

            window.Update();
        }
    }

    void Game::Shutdown() {
        SceneManager::ClearAll();
        ResourceManager::CleanUp();
        glfwTerminate();
    }

    void Game::LoadWindow(const std::string &windowOptionsPath) {
        glm::vec2 resolution;
        std::string title;

        IniFile file;
        file.ReadFile(windowOptionsPath.c_str());
        file.SetToSection("Window");
        file.RequireValue("resolution", resolution);
        file.RequireValue("title", title);

        Gfx::SetResolution(resolution.x, resolution.y);

        window.Generate(resolution.x, resolution.y, title);

        window.SetKeyCallback([](Window window, int key, int scancode, int action, int mods){
            Input::ProcessKeyInput(key, action);
        });

        window.SetCursorPosCallback([](Window window, double xPos, double yPos){
            Input::ProcessMouseMove(xPos, yPos);
        });

        window.SetMouseButtonCallback([](Window window, int button, int action, int mods){
            Input::ProcessMouseButton(button, action);
        });
    }

    void Game::LoadRenderingOptions(const std::string &renderingOptionsPath) {
        bool depthTest;
        bool blending;

        IniFile file;
        file.ReadFile(renderingOptionsPath.c_str());
        file.GetValue("depthTesting", depthTest, true);
        file.GetValue("blending", blending, true);

        if(depthTest) {
            Gfx::EnableDepthTest();
        } else {
            Gfx::DisableDepthTest();
        }

        if(blending) {
            Gfx::EnableBlending();
        } else {
            Gfx::DisableBlending();
        }
    }

    void Game::LoadResources(const std::string &resourceOptionsPath) {
        IniFile file;
        file.ReadFile(resourceOptionsPath.c_str());
        ResourceManager::LoadFromFile(file);
        ResourceManager::CreateUniformBuffer("Matrices", 3*sizeof(glm::mat4));
        TextRenderer::Init();
        Postprocessing::Init();
    }

    void Game::LoadInput(const std::string &inputOptionsPath) {
        Input::Initialize(&window);
        IniFile file;
        file.ReadFile(inputOptionsPath.c_str());
        Input::LoadFromFile(file);
    }

    void Game::LoadGame(const std::string &gameOptionsPath) {
        IniFile file;
        file.ReadFile(gameOptionsPath.c_str());
        file.SetToSection("Scenes");

        std::string startScene;

        file.GetValue("startScene", startScene);

        SceneManager::Init();
        SceneManager::SetStartingScene(startScene);
    }

    int Game::getFPS() {
        return lastFPS;
    }
}
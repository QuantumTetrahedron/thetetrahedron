
#include <TTH/Core/ColliderComponent.h>

#include "TTH/Core/ColliderComponent.h"
#include "Core_Out.h"

namespace TTH{
    ComponentRegister<ColliderComponent> ColliderComponent::reg("ColliderComponent");

    ColliderComponent::~ColliderComponent() {
        Raycaster::UnregisterWorldElement(this);
    }

    ColliderComponent *ColliderComponent::Clone() const {
        auto ret = new ColliderComponent();
        Raycaster::RegisterWorldElement(ret);
        return ret;
    }

    bool ColliderComponent::Collides(glm::vec2 point) {
        return false;
    }

    bool ColliderComponent::LoadFromFile(IniFile &file) {
        file.SetToSection("ColliderComponent");
        file.GetValue("scale", scale, glm::vec3(1.0f));
        return true;
    }

    void ColliderComponent::OnLoad() {
        Raycaster::RegisterWorldElement(this);
        components = parent->GetAllComponents<BehaviourComponent>();
        scale = scale * parent->transform.scale;
    }

    void ColliderComponent::OnMouseDown(int button) {
        for(BehaviourComponent* c : components){
            c->OnMouseDown(button);
        }
    }

    void ColliderComponent::OnMouseEnter() {
        for(BehaviourComponent* c : components){
            c->OnMouseEnter();
        }
    }

    void ColliderComponent::OnMouseLeave() {
        for(BehaviourComponent* c : components){
            c->OnMouseLeave();
        }
    }

    glm::vec3 ColliderComponent::GetScale() {
        return scale;
    }

    void ColliderComponent::SetScale(glm::vec3 newScale) {
        scale = newScale;
    }

}
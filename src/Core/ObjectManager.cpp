
#include <algorithm>
#include "TTH/Core/ObjectManager.h"

namespace TTH{
    PausableVector<Object*> ObjectManager::objects;

    void ObjectManager::AddObject(Object *o) {
        objects.push_back(o);
        //std::cout << "Added " << o->GetName() << std::endl;
    }

    void ObjectManager::Init() {
        objects.reserve(100);
    }

    void ObjectManager::Update(float dt) {
        for(Object* o : objects) {
            if (!o->IsDead() && o->IsActive()) {
                o->Update(dt);
            }
        }

        for(auto& o : objects){
            if(o->isDead){
                o->OnDestroy();
                //std::cout << "deleted " << o->GetName() << std::endl;
                delete o;
                o = nullptr;
            }
        }
        objects.erase(std::remove_if(objects.begin(), objects.end(), [](Object* obj){return obj == nullptr;}), objects.end());
        /*
        for(Object* o : objects){
            if(o->IsDead()){
                delete o;
                objects[i] = objects[objects.size()-1];
                objects.pop_back();
                --i;
            }
        }*/
    }

    void ObjectManager::Pause() {
        objects.Pause();
    }

    void ObjectManager::Resume() {
        objects.Resume();
    }

    void ObjectManager::DestroyAllObjects() {
        objects.MergeAll();
        DestroyObjects();
    }

    void ObjectManager::DestroyObjects() {
        for(Object* o : objects){
            o->OnDestroy();
            //std::cout << "deleted " << o->GetName() << std::endl;
            delete o;
            o = nullptr;
        }
        objects.clear();
    }

    Object *ObjectManager::GetObject(const std::string &name) {
        for(Object* o : objects){
            if(o->GetName() == name){
                return o;
            }
        }
        return nullptr;
    }

    void ObjectManager::Start() {
        for(Object* o : objects) {
            o->OnActive();
            o->Start();
        }
    }

    std::vector<Object *> ObjectManager::GetChildrenOf(Object *obj) {
        std::vector<Object*> ret;
        for(auto o : objects){
            if(o->parent == obj){
                ret.push_back(o);
            }
        }
        return ret;
    }
}
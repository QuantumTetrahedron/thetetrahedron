
#include "TTH/UI/HUDColliderComponent.h"
#include "UI_Out.h"

namespace TTH{
    ComponentRegister<HUDColliderComponent> HUDColliderComponent::reg("HUDColliderComponent");

    void HUDColliderComponent::OnLoad() {
        Raycaster::RegisterHUDElement(this);
        components = parent->GetAllComponents<BehaviourComponent>();
    }

    void HUDColliderComponent::OnMouseDown(int button) {
        for(BehaviourComponent* c : components){
            c->OnMouseDown(button);
        }
    }

    HUDColliderComponent::~HUDColliderComponent() {
        Raycaster::UnregisterHUDElement(this);
    }

    HUDColliderComponent *HUDColliderComponent::Clone() const {
        auto comp = new HUDColliderComponent();
        Raycaster::RegisterHUDElement(comp);
        return comp;
    }

    bool HUDColliderComponent::Collides(glm::vec2 point) {
        Transform pT = parent->GetWorldTransform();
        glm::vec2 pivotPoint = glm::vec2(pT.position);
        glm::vec2 p = pivotPoint - glm::vec2(pT.pivot * pT.scale);
        float r = -glm::radians(parent->GetWorldTransform().rotation.z);

        glm::vec2 rotatedPoint;
        rotatedPoint.x = glm::cos(r) * (point.x - pivotPoint.x) - glm::sin(r) * (point.y - pivotPoint.y) + pivotPoint.x;
        rotatedPoint.y = glm::sin(r) * (point.x - pivotPoint.x) + glm::cos(r) * (point.y - pivotPoint.y) + pivotPoint.y;

        return (rotatedPoint.x > p.x && rotatedPoint.x < p.x + pT.scale.x && rotatedPoint.y > p.y && rotatedPoint.y < p.y + pT.scale.y);
    }

    bool HUDColliderComponent::LoadFromFile(IniFile &file) {
        return true;
    }

    void HUDColliderComponent::OnMouseEnter() {
        for(BehaviourComponent* c : components){
            c->OnMouseEnter();
        }
    }

    void HUDColliderComponent::OnMouseLeave() {
        for(BehaviourComponent* c : components){
            c->OnMouseLeave();
        }
    }
}

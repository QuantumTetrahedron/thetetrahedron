
#include <TTH/UI/HUDComponent.h>

#include "TTH/UI/HUDComponent.h"
#include "UI_Out.h"

namespace TTH{
    bool HUDComponent::LoadFromFile(IniFile& file) {
        file.GetValue("anchor", anchor, glm::vec2(0.0f));
        return true;
    }

    void HUDComponent::OnLoad() {
        parent->transform.position.x += anchor.x;
        parent->transform.position.y += anchor.y;
    }

    void HUDComponent::SetAnchor(glm::vec2 newAnchor, bool keepPosition) {
        if(!keepPosition){
            parent->transform.position.x -= anchor.x;
            parent->transform.position.y -= anchor.y;

            parent->transform.position.x += newAnchor.x;
            parent->transform.position.y += newAnchor.y;
        }
        anchor = newAnchor;
    }

    glm::vec2 HUDComponent::GetAnchor() const {
        return anchor;
    }

    glm::vec2 HUDComponent::GetScreenPosition() const {
        Transform parentTransform = parent->GetWorldTransform();
        return glm::vec2(parentTransform.position - parentTransform.pivot * parentTransform.scale);
    }

    void HUDComponent::SetShader(const std::string &newShaderName) {
        shader = ResourceManager::GetShader(newShaderName);
    }
}

#include <TTH/UI/TextComponent.h>

#include "TTH/UI/TextComponent.h"
#include "UI_Out.h"

namespace TTH{
    ComponentRegister<TextComponent> TextComponent::reg("TextComponent");

    TextComponent::~TextComponent() {
        Gfx::UnregisterComponent(this);
    }

    void TextComponent::Draw() {
        shader->Use();
        shader->SetInt("text", 0);
        shader->SetVec3("colorTint", colorTint);
        Transform t = parent->GetWorldTransform();
        t.scale = parent->transform.scale;
        shader->SetMat4("model", t.GetModelMatrix());

        TextRenderer::Draw(text, font);
    }

    bool TextComponent::LoadFromFile(IniFile &file) {
        file.SetToSection("TextComponent");
        HUDComponent::LoadFromFile(file);
        file.GetValue("text", text);
        file.GetValue("colorTint", colorTint, glm::vec3(1.0f));
        std::string shaderName;
        file.RequireValue("shader", shaderName);
        shader = ResourceManager::GetShader(shaderName);
        std::string fontName;
        file.RequireValue("font", fontName);
        font = ResourceManager::GetFont(fontName);
        return true;
    }

    void TextComponent::OnLoad() {
        HUDComponent::OnLoad();
        Gfx::RegisterComponent(this);
    }

    Component *TextComponent::Clone() const {
        auto ret = new TextComponent();
        ret->text = text;
        ret->colorTint = colorTint;
        ret->shader = shader;
        ret->font = font;
        Gfx::RegisterComponent(ret);
        return ret;
    }

    void TextComponent::SetText(const std::string &newText) {
        text = newText;
    }

    void TextComponent::SetColor(glm::vec3 newColor) {
        colorTint = newColor;
    }

    void TextComponent::SetFont(const std::string &fontName) {
        font = ResourceManager::GetFont(fontName);
    }
}
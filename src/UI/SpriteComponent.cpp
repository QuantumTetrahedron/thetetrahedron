
#include <glad/glad.h>
#include "TTH/UI/SpriteComponent.h"
#include "UI_Out.h"

namespace TTH{

    ComponentRegister<SpriteComponent> SpriteComponent::reg("SpriteComponent");
    unsigned int SpriteComponent::VAO = 0;

    void SpriteComponent::Draw() {
        shader->Use();
        shader->SetInt("tex", 0);
        shader->SetVec3("colorTint", colorTint);
        shader->SetMat4("model", parent->GetWorldTransform().GetModelMatrix());

        glActiveTexture(GL_TEXTURE0);
        texture->Bind();
        glBindVertexArray(VAO);

        glDrawArrays(GL_TRIANGLES, 0, 6);
        glBindVertexArray(0);
    }

    bool SpriteComponent::LoadFromFile(IniFile &file) {
        file.SetToSection("SpriteComponent");
        HUDComponent::LoadFromFile(file);
        file.GetValue("colorTint", colorTint, glm::vec3(1.0f));
        std::string shaderName;
        file.RequireValue("shader", shaderName);
        shader = ResourceManager::GetShader(shaderName);
        std::string textureName;
        file.RequireValue("texture", textureName);
        texture = ResourceManager::GetTexture(textureName);
        return true;
    }

    void SpriteComponent::OnLoad() {
        HUDComponent::OnLoad();
        if(VAO == 0){
            float vertices[] = {
                    0.0f, 1.0f,
                    1.0f, 0.0f,
                    0.0f, 0.0f,
                    0.0f, 1.0f,
                    1.0f, 1.0f,
                    1.0f, 0.0f
            };

            glGenVertexArrays(1, &VAO);
            unsigned int VBO;
            glGenBuffers(1, &VBO);

            glBindVertexArray(VAO);
            glBindBuffer(GL_ARRAY_BUFFER, VBO);
            glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
            glVertexAttribPointer(0,2,GL_FLOAT,GL_FALSE,2*sizeof(float),0);
            glEnableVertexAttribArray(0);
            glBindBuffer(GL_ARRAY_BUFFER, 0);
            glBindVertexArray(0);
        }

        Gfx::RegisterComponent(this);
    }

    Component *SpriteComponent::Clone() const {
        auto ret = new SpriteComponent();
        ret->texture = texture;
        ret->colorTint = colorTint;
        ret->shader = shader;
        Gfx::RegisterComponent(ret);
        return ret;
    }

    SpriteComponent::~SpriteComponent() {
        Gfx::UnregisterComponent(this);
    }

    void SpriteComponent::SetTexture(Texture* newTex) {
        texture = newTex;
    }

    void SpriteComponent::SetTexture(std::string newTexName) {
        texture = ResourceManager::GetTexture(newTexName);
    }

    void SpriteComponent::SetColorTint(glm::vec3 newTint) {
        colorTint = newTint;
    }
}
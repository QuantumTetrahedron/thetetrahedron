
#include <iostream>
#include <sstream>
#include <fstream>
#include "TTH/Utils/IniFile.h"

namespace TTH{
    IniFile::IniFile(){
        sections.insert(std::make_pair("", std::map<std::string,std::string>()));
        SetToSection("");
    }

    void IniFile::ReadFile(const char *filePath) {
        currentSection = nullptr;
        sections.clear();
        std::ifstream file(filePath, std::ios::in);
        if(!file.is_open()){
            std::cerr << "Could not open file " << filePath << std::endl;
            return;
        }

        std::pair<std::string, std::map<std::string, std::string>> currSection("", std::map<std::string, std::string>());
        std::string line;
        while(getline(file, line)){
            size_t begin = 0;
            size_t end = line.length();

            RemoveLeadingSpaces(line, begin, end);

            if(begin >= end || line[begin] == ';') continue;

            if(line[begin] == '['){
                sections.insert(currSection);
                ParseSectionName(currSection, line, begin, end);
            }
            else
                ParseValue(currSection, line, begin, end);
        }
        sections.insert(currSection);
        file.close();
        SetToSection("");
    }

    void IniFile::SetToSection(const std::string& sectionName) {
        auto i = sections.find(sectionName);
        if(i == sections.end()){
            std::cerr << "Section " << sectionName << " does not exist." << std::endl;
            return;
        }
        currentSection = &i->second;
    }

    void IniFile::RemoveLeadingSpaces(const std::string &line, size_t &begin, size_t &end) const {
        if(begin >= end) return;
        while(line[begin] == ' ' || line[begin] == '\t'){
            if(begin == std::string::npos)break;
            ++begin;
        }
    }

    void IniFile::RemoveTrailingSpaces(const std::string &line, size_t &begin, size_t &end) const {
        if(begin >= end) return;
        while(line[end] == ' ' || line[end] == '\t'){
            --end;
        }
    }

    void IniFile::ParseSectionName(std::pair<std::string, std::map<std::string, std::string>> &currSection,
                                   std::string &line, size_t &begin, size_t &end) {
        GetSectionName(line, begin, end);
        currSection.first = line;
        currSection.second.clear();
    }

    void IniFile::ParseValue(std::pair<std::string, std::map<std::string, std::string>> &currSection, std::string &line,
                             size_t &begin, size_t &end) {
        std::pair<std::string, std::string> p;

        size_t keyEnd, valueBegin;
        keyEnd = valueBegin = line.find_first_of('=', begin);
        if(keyEnd == std::string::npos){
            std::cerr << "Error in file - no '=' found." << std::endl;
            return;
        }

        RemoveTrailingSpaces(line, begin, --keyEnd);
        RemoveLeadingSpaces(line, ++valueBegin, end);
        size_t commentBegin = line.find_first_of(';', valueBegin);
        if(commentBegin != std::string::npos)
            end = commentBegin;
        RemoveTrailingSpaces(line, valueBegin, --end);

        p.first = line.substr(begin, keyEnd - begin + 1);
        p.second = line.substr(valueBegin, end - valueBegin + 1);

        currSection.second.insert(p);
    }

    void IniFile::GetSectionName(std::string &line, size_t &begin, size_t &end) const {
        ++begin;
        RemoveLeadingSpaces(line, begin, end);
        end = line.find_first_of(']');
        if(begin == end || end == std::string::npos){
            std::cerr << "Invalid section name" << std::endl;
            return;
        }
        RemoveTrailingSpaces(line, begin, --end);
        line = line.substr(begin, end-begin+1);
    }

    IniFile IniFile::UpdateWith(IniFile &other) {
        IniFile file = *this;
        for(const auto& p : other.sections){
            std::string sectionName = p.first;

            if(file.sections.find(sectionName) == file.sections.end()){
                file.sections.insert(std::make_pair(sectionName, std::map<std::string, std::string>()));
            }

            file.SetToSection(sectionName);
            auto section = p.second;
            for(const auto& s : section){
                std::string paramName = s.first;
                std::string paramValue = s.second;

                auto it = file.currentSection->find(paramName);
                if(it != file.currentSection->end()){
                    it->second = paramValue;
                }
                else{
                    file.currentSection->insert(std::make_pair(paramName, paramValue));
                }
            }
        }
        return file;
    }
}

#include <glad/glad.h>
#include <iostream>
#include "TTH/Postprocessing/Framebuffer.h"
#include "Postprocessing_Out.h"

namespace TTH{

    Framebuffer::Renderer Framebuffer::renderer;

    Framebuffer::Framebuffer()
    : fbo(0), texture(nullptr){}

    Framebuffer::~Framebuffer() {
        if(texture){
            delete texture;
            texture = nullptr;
        }
        glDeleteFramebuffers(1, &fbo);
    }

    void Framebuffer::Generate(int width, int height, bool hdr) {
        w = width;
        h = height;
        glGenFramebuffers(1, &fbo);
        glBindFramebuffer(GL_FRAMEBUFFER, fbo);

        unsigned int tex;
        glGenTextures(1, &tex);
        glBindTexture(GL_TEXTURE_2D, tex);

        if(hdr){
            glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB16F, width, height, 0, GL_RGB, GL_FLOAT, nullptr);
        } else {
            glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, nullptr);
        }

        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glBindTexture(GL_TEXTURE_2D, 0);

        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, tex, 0);

        unsigned int rbo;
        glGenRenderbuffers(1, &rbo);
        glBindRenderbuffer(GL_RENDERBUFFER, rbo);
        glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH24_STENCIL8, width, height);
        glBindRenderbuffer(GL_RENDERBUFFER,0);

        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_RENDERBUFFER, rbo);

        if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE){
            std::cerr << "Framebuffer error" << std::endl;
        }
        glBindFramebuffer(GL_FRAMEBUFFER, 0);

        if(texture){
            delete texture;
            texture = nullptr;
        }
        texture = new Texture(tex);

        if(!renderer.isInitialized())
            renderer.Init();
    }

    void Framebuffer::BindAsTarget() {
        glBindFramebuffer(GL_FRAMEBUFFER, fbo);
        glViewport(0,0,w,h);
        Gfx::SetResolution(w,h);
    }

    void Framebuffer::Unbind() {
        glBindFramebuffer(GL_FRAMEBUFFER, 0);
    }

    void Framebuffer::Display() const {
        renderer.Draw(this);
    }

    Texture* Framebuffer::getTexture() {
        return texture;
    }

    void Framebuffer::Renderer::Draw(const Framebuffer* framebuffer) {
        //glDisable(GL_DEPTH_TEST);
        glClear(GL_DEPTH_BUFFER_BIT);
        glBindVertexArray(VAO);
        glActiveTexture(GL_TEXTURE0);
        framebuffer->texture->Bind();
        glDrawArrays(GL_TRIANGLES,0,6);
        glBindTexture(GL_TEXTURE_2D,0);
        glBindVertexArray(0);
        //glEnable(GL_DEPTH_TEST);
    }

    void Framebuffer::Renderer::Init() {
        float quadVertices[] = {
                -1.0f,  1.0f,  0.0f, 1.0f,
                -1.0f, -1.0f,  0.0f, 0.0f,
                1.0f, -1.0f,  1.0f, 0.0f,

                -1.0f,  1.0f,  0.0f, 1.0f,
                1.0f, -1.0f,  1.0f, 0.0f,
                1.0f,  1.0f,  1.0f, 1.0f
        };

        glGenVertexArrays(1, &VAO);
        glGenBuffers(1, &VBO);
        glBindVertexArray(VAO);
        glBindBuffer(GL_ARRAY_BUFFER, VBO);
        glBufferData(GL_ARRAY_BUFFER, sizeof(quadVertices), &quadVertices, GL_STATIC_DRAW);
        glVertexAttribPointer(0,2,GL_FLOAT, GL_FALSE, 4*sizeof(float), (void*)0);
        glEnableVertexAttribArray(0);
        glVertexAttribPointer(1,2,GL_FLOAT, GL_FALSE, 4*sizeof(float), (void*)(2*sizeof(float)));
        glEnableVertexAttribArray(1);
        glBindVertexArray(0);
        initialized = true;
    }

    bool Framebuffer::Renderer::isInitialized() const {
        return initialized;
    }
}

#include "TTH/Postprocessing/Postprocessing.h"
#include "Postprocessing_Out.h"

namespace TTH{
    Framebuffer Postprocessing::offScreen;
    Shader* Postprocessing::offScreenShader;
    int Postprocessing::screenWidth;
    int Postprocessing::screenHeight;

    void Postprocessing::Process(Framebuffer &framebuffer, std::string effectName) {
        Shader* shader = ResourceManager::GetPostprocessEffect(effectName);

        offScreenShader->Use();
        offScreenShader->SetInt("screenTexture", 0);
        offScreenShader->SetInt("screenWidth", screenWidth);
        offScreenShader->SetInt("screenHeight", screenHeight);

        offScreen.BindAsTarget();
        framebuffer.Display();

        framebuffer.BindAsTarget();

        shader->Use();
        shader->SetInt("screenTexture", 0);
        shader->SetInt("screenWidth", screenWidth);
        shader->SetInt("screenHeight", screenHeight);

        offScreen.Display();

        framebuffer.Unbind();
    }

    void Postprocessing::ProcessTo(Framebuffer &from, Framebuffer& to, const std::string& effectName){
        to.BindAsTarget();
        Shader* shader = ResourceManager::GetPostprocessEffect(effectName);
        shader->Use();
        shader->SetInt("screenTexture", 0);
        shader->SetInt("screenWidth", screenWidth);
        shader->SetInt("screenHeight", screenHeight);
        from.Display();
        to.Unbind();
    }

    void Postprocessing::Init() {
        Gfx::GetResolution(&screenWidth, &screenHeight);
        offScreen.Generate(screenWidth,screenHeight);
        offScreenShader = ResourceManager::GetShader("framebuffer");
    }
}

#include <iostream>
#include <glad/glad.h>
#include "TTH/Resources/Model.h"
#include "TTH/Utils/stb_image.h"


namespace TTH{
    Model::Model(const std::string& filePath) {
        LoadModel(filePath);
    }

    void Model::Draw(const Shader &shader) const {
        for(const Mesh& mesh : meshes)
            mesh.Draw(shader);
    }

    void Model::LoadModel(const std::string& filePath) {
        Assimp::Importer importer;
        const aiScene* scene = importer.ReadFile(filePath, aiProcess_Triangulate | aiProcess_FlipUVs | aiProcess_CalcTangentSpace);
        if(!scene || scene->mFlags == AI_SCENE_FLAGS_INCOMPLETE || !scene->mRootNode)
        {
            std::cerr << "Assimp error: " << importer.GetErrorString() << std::endl;
            return;
        }
        directory = filePath.substr(0,filePath.find_last_of('/'));
        ProcessNode(scene->mRootNode, scene);
    }

    void Model::ProcessNode(aiNode *node, const aiScene *scene) {
        for(unsigned int i = 0; i<node->mNumMeshes; ++i)
        {
            aiMesh* mesh = scene->mMeshes[node->mMeshes[i]];
            meshes.push_back(ProcessMesh(mesh, scene));
        }

        for(unsigned int i=0; i<node->mNumChildren; ++i)
            ProcessNode(node->mChildren[i], scene);
    }

    Mesh Model::ProcessMesh(aiMesh *mesh, const aiScene *scene) {
        std::vector<Mesh::Vertex> vertices;
        std::vector<unsigned int> indices;
        std::vector<Mesh::Tex> texs;

        for(unsigned int i=0; i<mesh->mNumVertices; ++i){
            Mesh::Vertex vertex;
            glm::vec3 vector;

            vector.x = mesh->mVertices[i].x;
            vector.y = mesh->mVertices[i].y;
            vector.z = mesh->mVertices[i].z;
            vertex.Position = vector;

            vector.x = mesh->mNormals[i].x;
            vector.y = mesh->mNormals[i].y;
            vector.z = mesh->mNormals[i].z;
            vertex.Normal = vector;

            vector.x = mesh->mTangents[i].x;
            vector.y = mesh->mTangents[i].y;
            vector.z = mesh->mTangents[i].z;
            vertex.Tangent = vector;

            vector.x = mesh->mBitangents[i].x;
            vector.y = mesh->mBitangents[i].y;
            vector.z = mesh->mBitangents[i].z;
            vertex.Bitangent = vector;

            if(mesh->mTextureCoords[0]){
                glm::vec2 vec;
                vec.x = mesh->mTextureCoords[0][i].x;
                vec.y = mesh->mTextureCoords[0][i].y;
                vertex.TexCoords = vec;
            }else{
                vertex.TexCoords = glm::vec2(0.0f);
            }
            vertices.push_back(vertex);
        }

        for(unsigned int i = 0; i<mesh->mNumFaces; ++i)
        {
            aiFace face = mesh->mFaces[i];
            for(unsigned int j = 0; j < face.mNumIndices; ++j){
                indices.push_back(face.mIndices[j]);
            }
        }

        aiMaterial* material = scene->mMaterials[mesh->mMaterialIndex];

        std::vector<Mesh::Tex> diffuseMaps = LoadMaterialTextures(material, aiTextureType_DIFFUSE, "texture_diffuse", false);
        texs.insert(texs.end(), diffuseMaps.begin(), diffuseMaps.end());

        std::vector<Mesh::Tex> specularMaps = LoadMaterialTextures(material, aiTextureType_SPECULAR, "texture_specular", false);
        texs.insert(texs.end(), specularMaps.begin(), specularMaps.end());

        std::vector<Mesh::Tex> normalMaps = LoadMaterialTextures(material, aiTextureType_HEIGHT, "texture_normal", false);
        texs.insert(texs.end(), normalMaps.begin(), normalMaps.end());

        std::vector<Mesh::Tex> heightMaps = LoadMaterialTextures(material, aiTextureType_AMBIENT, "texture_height", false);
        texs.insert(texs.end(), heightMaps.begin(), heightMaps.end());

        return Mesh(vertices, indices, texs);
    }

    std::vector<Mesh::Tex> Model::LoadMaterialTextures(aiMaterial *mat, aiTextureType type, std::string typeName, bool gammaCorrection) {
        std::vector<Mesh::Tex> texs;

        for(unsigned int i=0; i<mat->GetTextureCount(type); ++i){
            aiString str;
            mat->GetTexture(type, i, &str);
            bool skip = false;
            for(unsigned int j = 0; j<textures.size(); ++j){
                if(std::strcmp(textures[j].path.C_Str(), str.C_Str()) == 0){
                    texs.push_back(textures[j]);
                    skip = true;
                    break;
                }
            }
            if(!skip){
                Mesh::Tex texture;
                texture.id = TextureFromFile(str.C_Str(), directory, gammaCorrection);
                texture.type = typeName;
                texture.path = str;
                texs.push_back(texture);
                textures.push_back(texture);
            }
        }
        return texs;
    }

    unsigned int Model::TextureFromFile(const char *path, const std::string &directory, bool gammaCorrection) {
        std::string filename = directory + "/" + path;

        unsigned int texID;
        glGenTextures(1, &texID);

        int width, height, nrChannels;
        unsigned char* data = stbi_load(filename.c_str(), &width, &height, &nrChannels, 0);
        if(data){
            GLenum internalFormat, dataFormat;
            if(nrChannels == 1)
                internalFormat = dataFormat = GL_RED;
            else if(nrChannels == 3){
                internalFormat = gammaCorrection ? GL_SRGB : GL_RGB;
                dataFormat = GL_RGB;
            }
            else if(nrChannels == 4){
                internalFormat = gammaCorrection ? GL_SRGB_ALPHA : GL_RGBA;
                dataFormat = GL_RGBA;
            }else return 0;

            glBindTexture(GL_TEXTURE_2D, texID);
            glTexImage2D(GL_TEXTURE_2D, 0, internalFormat, width, height, 0, dataFormat, GL_UNSIGNED_BYTE, data);
            glGenerateMipmap(GL_TEXTURE_2D);

            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        }
        else std::cerr << "Texture failed to load: " << path << std::endl;

        stbi_image_free(data);
        return texID;
    }
}
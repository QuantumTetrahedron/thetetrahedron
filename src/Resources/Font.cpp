
#include "TTH/Resources/Font.h"
#include <glad/glad.h>

namespace TTH{
    Font::Character Font::operator[](char c){
        return characters[c];
    }

    Font::Character Font::at(char c) const{
        return characters.at(c);
    }

    Font::Font(const std::string& font, unsigned int fontSize){
        FT_Library ft;
        if(FT_Init_FreeType(&ft)){
            std::cerr << "Could not init freetype" << std::endl;
        }
        FT_Face face;
        if(FT_New_Face(ft, font.c_str(), 0, &face)){
            std::cerr << "Failed to load font " << font << std::endl;
        }
        FT_Set_Pixel_Sizes(face,0,fontSize);
        glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
        for(unsigned char c = 0; c<128; ++c){
            if(FT_Load_Char(face, c, FT_LOAD_RENDER)){
                std::cerr << "Failed to load glyph " << c << std::endl;
                continue;
            }
            unsigned int tex;
            glGenTextures(1, &tex);
            glBindTexture(GL_TEXTURE_2D, tex);
            glTexImage2D(GL_TEXTURE_2D, 0, GL_RED, face->glyph->bitmap.width, face->glyph->bitmap.rows, 0, GL_RED, GL_UNSIGNED_BYTE, face->glyph->bitmap.buffer);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

            Character character = {
                    tex,
                    glm::ivec2(face->glyph->bitmap.width, face->glyph->bitmap.rows),
                    glm::ivec2(face->glyph->bitmap_left, face->glyph->bitmap_top),
                    face->glyph->advance.x
            };
            characters.emplace(c, character);
        }
        glBindTexture(GL_TEXTURE_2D, 0);
        FT_Done_Face(face);
        FT_Done_FreeType(ft);
    }
}

//
// Created by bartek on 21.12.2019.
//

#include <glad/glad.h>
#include <TTH/Resources/UniformBuffer.h>

namespace TTH{

    unsigned int UniformBuffer::nextId;

    UniformBuffer::UniformBuffer(const std::string& name, unsigned int size) {
        this->name = name;
        glGenBuffers(1, &ubo);
        glBindBuffer(GL_UNIFORM_BUFFER, ubo);
        glBufferData(GL_UNIFORM_BUFFER, size, NULL, GL_STATIC_DRAW);
        glBindBuffer(GL_UNIFORM_BUFFER, 0);
        bindingPoint = nextId;
        nextId++;

        glBindBufferBase(GL_UNIFORM_BUFFER, bindingPoint, ubo);
    }

    void UniformBuffer::LinkShader(Shader *shader) {
        unsigned int index = glGetUniformBlockIndex(shader->id, name.c_str());
        glUniformBlockBinding(shader->id, index, bindingPoint);
    }

    void UniformBuffer::SetInt(int value, unsigned int offset) {
        glBindBuffer(GL_UNIFORM_BUFFER, ubo);
        glBufferSubData(GL_UNIFORM_BUFFER, offset, sizeof(int), &value);
        glBindBuffer(GL_UNIFORM_BUFFER, 0);
    }

    void UniformBuffer::SetFloat(float value, unsigned int offset) {
        glBindBuffer(GL_UNIFORM_BUFFER, ubo);
        glBufferSubData(GL_UNIFORM_BUFFER, offset, sizeof(float), &value);
        glBindBuffer(GL_UNIFORM_BUFFER, 0);
    }

    void UniformBuffer::SetVec2(glm::vec2 value, unsigned int offset) {
        glBindBuffer(GL_UNIFORM_BUFFER, ubo);
        glBufferSubData(GL_UNIFORM_BUFFER, offset, sizeof(glm::vec2), &value[0]);
        glBindBuffer(GL_UNIFORM_BUFFER, 0);
    }

    void UniformBuffer::SetVec3(glm::vec3 value, unsigned int offset) {
        glBindBuffer(GL_UNIFORM_BUFFER, ubo);
        glBufferSubData(GL_UNIFORM_BUFFER, offset, sizeof(glm::vec3), &value[0]);
        glBindBuffer(GL_UNIFORM_BUFFER, 0);
    }

    void UniformBuffer::SetVec4(glm::vec4 value, unsigned int offset) {
        glBindBuffer(GL_UNIFORM_BUFFER, ubo);
        glBufferSubData(GL_UNIFORM_BUFFER, offset, sizeof(glm::vec4), &value[0]);
        glBindBuffer(GL_UNIFORM_BUFFER, 0);
    }

    void UniformBuffer::SetMat4(glm::mat4 value, unsigned int offset) {
        glBindBuffer(GL_UNIFORM_BUFFER, ubo);
        glBufferSubData(GL_UNIFORM_BUFFER, offset, sizeof(glm::mat4), &value[0]);
        glBindBuffer(GL_UNIFORM_BUFFER, 0);
    }
}

#include <glad/glad.h>
#include <fstream>
#include <sstream>
#include <iostream>
#include "TTH/Resources/Shader.h"

namespace TTH {
    Shader::Shader(const char *vertexFilePath, const char *fragmentFilePath) {
        LoadFromFile(vertexFilePath, fragmentFilePath);
    }

    void Shader::Use() const {
        glUseProgram(id);
    }

    void Shader::SetBool(const std::string &name, bool value) const {
        glUniform1i(glGetUniformLocation(id, name.c_str()), (int) value);
    }

    void Shader::SetInt(const std::string &name, int value) const {
        glUniform1i(glGetUniformLocation(id, name.c_str()), value);
    }

    void Shader::SetFloat(const std::string &name, float value) const {
        glUniform1f(glGetUniformLocation(id, name.c_str()), value);
    }

    void Shader::SetVec2(const std::string &name, const glm::vec2 &v) const {
        glUniform2fv(glGetUniformLocation(id, name.c_str()), 1, &v[0]);
    }

    void Shader::SetVec2(const std::string &name, float x, float y) const {
        glUniform2f(glGetUniformLocation(id, name.c_str()), x, y);
    }

    void Shader::SetVec3(const std::string &name, const glm::vec3 &v) const {
        glUniform3fv(glGetUniformLocation(id, name.c_str()), 1, &v[0]);
    }

    void Shader::SetVec3(const std::string &name, float x, float y, float z) const {
        glUniform3f(glGetUniformLocation(id, name.c_str()), x, y, z);
    }

    void Shader::SetVec4(const std::string &name, const glm::vec4 &v) const {
        glUniform4fv(glGetUniformLocation(id, name.c_str()), 1, &v[0]);
    }

    void Shader::SetVec4(const std::string &name, float x, float y, float z, float w) const {
        glUniform4f(glGetUniformLocation(id, name.c_str()), x, y, z, w);
    }

    void Shader::SetMat4(const std::string &name, const glm::mat4 &mat) const {
        glUniformMatrix4fv(glGetUniformLocation(id, name.c_str()), 1, GL_FALSE, &mat[0][0]);
    }

    void Shader::LoadFromFile(const char *vertexFilePath, const char *fragmentFilePath) {
        std::string vertexCode;
        std::string fragmentCode;
        std::ifstream vShaderFile;
        std::ifstream fShaderFile;
        vShaderFile.exceptions(std::ifstream::failbit | std::ifstream::badbit);
        fShaderFile.exceptions(std::ifstream::failbit | std::ifstream::badbit);
        try {
            vShaderFile.open(vertexFilePath);
            fShaderFile.open(fragmentFilePath);
            std::stringstream vShaderStream, fShaderStream;
            vShaderStream << vShaderFile.rdbuf();
            fShaderStream << fShaderFile.rdbuf();
            vShaderFile.close();
            fShaderFile.close();
            vertexCode = vShaderStream.str();
            fragmentCode = fShaderStream.str();
        } catch (std::ifstream::failure e) {
            std::cerr << "Error - Reading shader file failed." << std::endl;
        }

        const char *vShaderCode = vertexCode.c_str();
        const char *fShaderCode = fragmentCode.c_str();
        unsigned int vertex, fragment;

        vertex = glCreateShader(GL_VERTEX_SHADER);
        glShaderSource(vertex, 1, &vShaderCode, nullptr);
        glCompileShader(vertex);
        checkErrors(vertex, "vertex");

        fragment = glCreateShader(GL_FRAGMENT_SHADER);
        glShaderSource(fragment, 1, &fShaderCode, nullptr);
        glCompileShader(fragment);
        checkErrors(fragment, "fragment");

        id = glCreateProgram();
        glAttachShader(id, vertex);
        glAttachShader(id, fragment);
        glLinkProgram(id);
        checkErrors(id, "program");

        glDeleteShader(vertex);
        glDeleteShader(fragment);
    }

    void Shader::checkErrors(unsigned int shader, std::string type) {
        int success;
        char infoLog[1024];
        if(type != "program"){
            glGetShaderiv(shader, GL_COMPILE_STATUS, &success);
            if(!success){
                glGetShaderInfoLog(shader, 1024, nullptr, infoLog);
                std::cerr << "Error - Shader compilation error of type " << type << std::endl;
                std::cerr << infoLog << std::endl;
            }
        }
        else{
            glGetProgramiv(shader, GL_LINK_STATUS, &success);
            if(!success){
                glGetProgramInfoLog(shader, 1024, nullptr, infoLog);
                std::cerr << "Error - Shader linking error:" << std::endl;
                std::cerr << infoLog << std::endl;
            }
        }
    }
}
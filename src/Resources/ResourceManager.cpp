
#include "TTH/Resources/ResourceManager.h"
#include "TTH/Utils/IniFile.h"

namespace TTH {
    std::map<std::string, Shader*> ResourceManager::shaders;
    std::map<std::string, Model*> ResourceManager::models;
    std::map<std::string, Texture*> ResourceManager::textures;
    std::map<std::string, Font*> ResourceManager::fonts;
    std::map<std::string, Shader*> ResourceManager::postprocessEffects;
    std::map<std::string, UniformBuffer*> ResourceManager::uniformBuffers;

    Shader* ResourceManager::GetShader(const std::string &name) {
        return shaders.at(name);
    }

    void ResourceManager::LoadShader(const char *vertexShaderFilePath, const char *fragmentShaderFilePath, const std::string &name) {
        auto it = shaders.find(name);
        if(it == shaders.end()) {
            auto shader = new Shader(vertexShaderFilePath, fragmentShaderFilePath);
            shaders.emplace(name, shader);
        }
    }

    void ResourceManager::DeleteShader(const std::string &name) {
        auto it = shaders.find(name);
        if(it != shaders.end()){
            delete it->second;
            it->second = nullptr;
            shaders.erase(it);
        }
    }

    Model* ResourceManager::GetModel(const std::string &name) {
        return models.at(name);
    }

    void ResourceManager::LoadModel(const char *filePath, const std::string &name) {
        auto it = models.find(name);
        if(it == models.end()) {
            auto model = new Model(filePath);
            models.emplace(name, model);
        }
    }

    void ResourceManager::DeleteModel(const std::string &name) {
        auto it = models.find(name);
        if(it != models.end()){
            delete it->second;
            it->second = nullptr;
            models.erase(it);
        }
    }

    void ResourceManager::LoadFromFile(IniFile& file) {
        file.SetToSection("Shaders");
        std::string shadersList;
        file.GetValue("shaders", shadersList);
        std::string s;
        std::stringstream shaders_ss(shadersList);
        while(shaders_ss >> s){
            std::string shaderData;
            file.GetValue(s, shaderData);
            std::stringstream data_ss(shaderData);
            std::string vertexFilePath, fragmentFilePath;
            data_ss >> vertexFilePath >> fragmentFilePath;
            LoadShader(vertexFilePath.c_str(), fragmentFilePath.c_str(), s);
        }

        file.SetToSection("Models");
        std::string modelsList;
        file.GetValue("models", modelsList);
        std::stringstream models_ss(modelsList);
        while(models_ss >> s){
            std::string modelData;
            file.GetValue(s, modelData);
            LoadModel(modelData.c_str(), s);
        }

        file.SetToSection("Textures");
        std::string texturesList;
        file.GetValue("textures", texturesList);
        std::stringstream textures_ss(texturesList);
        while(textures_ss >> s){
            std::string textureData;
            file.GetValue(s, textureData);
            LoadTexture(textureData.c_str(), s);
        }

        file.SetToSection("Fonts");
        std::string fontsList;
        file.GetValue("fonts", fontsList);
        std::stringstream fonts_ss(fontsList);
        while(fonts_ss >> s){
            std::string fontData;
            file.GetValue(s, fontData);
            std::string path;
            unsigned int size;
            std::stringstream ss(fontData);
            ss >> path >> size;
            LoadFont(path.c_str(), size, s);
        }


        file.SetToSection("Postprocessing");
        std::string shaders;
        file.GetValue("shaders", shaders);
        std::stringstream post_ss(shaders);
        while(post_ss >> s){
            std::string shaderData;
            file.GetValue(s, shaderData);
            std::stringstream data_ss(shaderData);
            std::string vertexFilePath, fragmentFilePath;
            data_ss >> vertexFilePath >> fragmentFilePath;
            LoadPostprocessEffect(vertexFilePath.c_str(), fragmentFilePath.c_str(), s);
        }
    }

    Texture *ResourceManager::GetTexture(const std::string &name) {
        return textures.at(name);
    }

    void ResourceManager::LoadTexture(const char *filePath, const std::string &name) {
        auto it = textures.find(name);
        if(it == textures.end()){
            auto texture = new Texture(filePath);
            textures.emplace(name, texture);
        }
    }

    void ResourceManager::DeleteTexture(const std::string &name) {
        auto it = textures.find(name);
        if(it != textures.end()){
            delete it->second;
            it->second = nullptr;
            textures.erase(it);
        }
    }

    Font *ResourceManager::GetFont(const std::string &name) {
        return fonts.at(name);
    }

    void ResourceManager::LoadFont(const char *filePath, unsigned int size, const std::string &name) {
        auto it = fonts.find(name);
        if(it == fonts.end()){
            auto font = new Font(filePath, size);
            fonts.emplace(name, font);
        }
    }

    void ResourceManager::DeleteFont(const std::string &name) {
        auto it = fonts.find(name);
        if(it != fonts.end()){
            delete it->second;
            it->second = nullptr;
            fonts.erase(it);
        }
    }

    void ResourceManager::CleanUp() {
        FreeMap(shaders);
        FreeMap(models);
        FreeMap(textures);
        FreeMap(fonts);
        FreeMap(postprocessEffects);
    }

    Shader *ResourceManager::GetPostprocessEffect(const std::string &name) {
        return postprocessEffects.at(name);
    }

    void ResourceManager::LoadPostprocessEffect(const char *vertexShaderPath, const char *fragmentShaderPath,
                                                const std::string &name) {
        auto it = postprocessEffects.find(name);
        if(it == postprocessEffects.end()){
            auto shader = new Shader(vertexShaderPath, fragmentShaderPath);
            postprocessEffects.emplace(name, shader);
        }
    }

    void ResourceManager::DeletePostprocessEffect(const std::string &name) {
        auto it = postprocessEffects.find(name);
        if(it!=postprocessEffects.end()){
            delete it->second;
            it->second = nullptr;
            postprocessEffects.erase(it);
        }
    }


    void ResourceManager::CreateUniformBuffer(std::string name, unsigned int size) {
        auto it = uniformBuffers.find(name);
        if(it==uniformBuffers.end()) {
            auto ubo = new UniformBuffer(name, size);
            for (auto &p : shaders) {
                Shader *shader = p.second;
                ubo->LinkShader(shader);
            }
            for(auto &p : postprocessEffects){
                Shader* shader = p.second;
                ubo->LinkShader(shader);
            }
            uniformBuffers.emplace(name, ubo);
        }
    }

    UniformBuffer* ResourceManager::GetUniformBuffer(std::string name) {
        return uniformBuffers.at(name);
    }

    void ResourceManager::DeleteUniformBuffer(std::string name) {
        auto it = uniformBuffers.find(name);
        if(it!=uniformBuffers.end()){
            delete it->second;
            it->second = nullptr;
            uniformBuffers.erase(it);
        }
    }

    bool ResourceManager::HasModel(const std::string &name) {
        return models.find(name) != models.end();
    }
}
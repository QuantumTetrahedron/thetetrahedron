
#define STB_IMAGE_IMPLEMENTATION
#include "TTH/Utils/stb_image.h"

#include "TTH/Resources/Texture.h"

#include <glad/glad.h>
#include <iostream>

namespace TTH{

    Texture::Texture(const char *path)
    : width(0), height(0), dataFormat(GL_RGB), internalFormat(GL_RGB),
      wrapS(GL_REPEAT), wrapT(GL_REPEAT), filterMin(GL_LINEAR_MIPMAP_LINEAR), filterMag(GL_LINEAR) {
        stbi_set_flip_vertically_on_load(true);
        unsigned char *data = stbi_load(path, &width, &height, &nrChannels, 0);
        glGenTextures(1, &id);
        glBindTexture(GL_TEXTURE_2D, id);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, wrapS);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, wrapT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, filterMin);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, filterMag);
        if(data){
            // TODO : SRGB, SRGB_ALPHA
            dataFormat = nrChannels == 1 ? GL_RED : nrChannels == 2 ? GL_RG : nrChannels == 3 ? GL_RGB : GL_RGBA;
            internalFormat = dataFormat;
            glTexImage2D(GL_TEXTURE_2D, 0, internalFormat, width, height, 0, dataFormat, GL_UNSIGNED_BYTE, data);
            glGenerateMipmap(GL_TEXTURE_2D);
        }
        else std::cerr << "Failed to load image" << std::endl;
        glBindTexture(GL_TEXTURE_2D, 0);
        stbi_image_free(data);
    }

    void Texture::Bind() const {
        glBindTexture(GL_TEXTURE_2D, id);
    }

    Texture::Texture(unsigned int _id) {
        id = _id;
    }

    Texture::~Texture() {
        glDeleteTextures(1, &id);
    }
}
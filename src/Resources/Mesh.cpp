
#include <glad/glad.h>
#include <sstream>
#include "TTH/Resources/Mesh.h"

namespace TTH{

    Mesh::Mesh(const std::vector<Vertex> &vertices, const std::vector<unsigned int> &indices, const std::vector<Tex> &textures)
    : vertices(vertices), indices(indices), textures(textures){
        SetupMesh();
    }

    void Mesh::Draw(const Shader &shader) const {
        SetupDraw(shader);
        shader.Use();
        glBindVertexArray(VAO);

        glDrawElements(GL_TRIANGLES, indices.size(), GL_UNSIGNED_INT, 0);
        glBindVertexArray(0);

        for(unsigned int i = 0; i < textures.size(); ++i){
            glActiveTexture(GL_TEXTURE0+i);
            glBindTexture(GL_TEXTURE_2D, 0);
        }
    }

    void Mesh::SetupDraw(const Shader &shader) const {
        shader.Use();
        unsigned int diffuseNr = 1;
        unsigned int specularNr = 1;
        unsigned int normalNr = 1;
        unsigned int heightNr = 1;

        for(unsigned int i = 0; i < textures.size(); ++i){
            glActiveTexture(GL_TEXTURE0 + i);
            std::stringstream ss;
            std::string number;
            std::string name = textures[i].type;
            if(name == "texture_diffuse")
                ss << diffuseNr++;
            else if(name == "texture_specular")
                ss << specularNr++;
            else if(name == "texture_normal")
                ss << normalNr++;
            else if(name == "texture_height")
                ss << heightNr++;

            number = ss.str();
            shader.SetInt(name+number, i);
            glBindTexture(GL_TEXTURE_2D, textures[i].id);
        }
    }

    void Mesh::SetupMesh() {
        glGenVertexArrays(1, &VAO);
        glGenBuffers(1, &VBO);
        glGenBuffers(1, &EBO);

        glBindVertexArray(VAO);
        glBindBuffer(GL_ARRAY_BUFFER, VBO);
        glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(Vertex), &vertices[0], GL_STATIC_DRAW);

        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(unsigned int), &indices[0], GL_STATIC_DRAW);

        glVertexAttribPointer(0,3,GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)0);
        glEnableVertexAttribArray(0);

        glVertexAttribPointer(1,2,GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)(offsetof(Vertex, TexCoords)));
        glEnableVertexAttribArray(1);

        glVertexAttribPointer(2,3,GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)(offsetof(Vertex, Normal)));
        glEnableVertexAttribArray(2);

        glVertexAttribPointer(3,3,GL_FLOAT,GL_FALSE,sizeof(Vertex), (void*)(offsetof(Vertex, Tangent)));
        glEnableVertexAttribArray(3);

        glVertexAttribPointer(4,3,GL_FLOAT,GL_FALSE,sizeof(Vertex), (void*)(offsetof(Vertex, Bitangent)));
        glEnableVertexAttribArray(4);

        glBindVertexArray(0);
    }
}
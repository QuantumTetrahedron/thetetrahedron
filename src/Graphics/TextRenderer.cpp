
#include <glad/glad.h>
#include "TTH/Graphics/TextRenderer.h"
#include "Graphics_Out.h"

namespace TTH{
    unsigned int TextRenderer::VAO = 0;
    unsigned int TextRenderer::VBO = 0;

    void TextRenderer::Init() {
        glGenVertexArrays(1, &VAO);
        glGenBuffers(1, &VBO);
        glBindVertexArray(VAO);
        glBindBuffer(GL_ARRAY_BUFFER, VBO);
        glBufferData(GL_ARRAY_BUFFER, sizeof(float)*24, NULL, GL_DYNAMIC_DRAW);
        glEnableVertexAttribArray(0);
        glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 4*sizeof(float), 0);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        glBindVertexArray(0);
    }

    void TextRenderer::Draw(const std::string& text, Font *font){
        glActiveTexture(GL_TEXTURE0);
        glBindVertexArray(VAO);
        std::string::const_iterator c;
        float x = 0.0f;
        for(c = text.begin(); c!=text.end(); ++c){
            Font::Character ch = (*font)[*c];
            float xPos = x+ch.Bearing.x;
            float yPos = ((*font)['H'].Bearing.y-ch.Bearing.y);
            float w = ch.Size.x;
            float h = ch.Size.y;
            float vertices[6][4] = {
                    {xPos, yPos+h, 0.0f, 1.0f},
                    {xPos+w, yPos, 1.0f, 0.0f},
                    {xPos, yPos, 0.0f, 0.0f},
                    {xPos, yPos+h, 0.0f, 1.0f},
                    {xPos+w, yPos+h, 1.0f, 1.0f},
                    {xPos+w, yPos, 1.0f, 0.0f}
            };

            glBindTexture(GL_TEXTURE_2D, ch.TextureID);
            glBindBuffer(GL_ARRAY_BUFFER, VBO);
            glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(vertices), vertices);
            glBindBuffer(GL_ARRAY_BUFFER, 0);
            glDrawArrays(GL_TRIANGLES, 0, 6);
            x += (ch.Advance >> 6);
        }
        glBindVertexArray(0);
        glBindTexture(GL_TEXTURE_2D,0);
    }


}
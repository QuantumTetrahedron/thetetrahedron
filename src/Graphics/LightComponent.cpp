
#include <TTH/Graphics/LightComponent.h>

#include "TTH/Graphics/LightComponent.h"
#include "TTH/Graphics/Gfx.h"
#include "Graphics_Out.h"

namespace TTH {


    LightComponent::~LightComponent() {
        Gfx::UnregisterLight(this);
    }

    void LightComponent::OnLoad() {
        Gfx::RegisterLight(this);
    }
}

/*
#include <TTH/Graphics/LightComponent.h>
#include "TTH/Graphics/Gfx.h"
#include "Graphics_Out.h"

namespace TTH{
    ComponentRegister<LightComponent> LightComponent::reg("LightComponent");

    void LightComponent::SetUniforms(Shader* shader) {
        shader->Use();
        shader->SetVec3(name+".position", parent->GetWorldTransform().position);
        shader->SetVec3(name+".color", color);
    }

    bool LightComponent::LoadFromFile(IniFile &file) {
        file.SetToSection("LightComponent");

        file.RequireValue("name", name);
        file.GetValue("color", color, glm::vec3(1.0f));

        return true;
    }

    LightComponent *LightComponent::Clone() const {
        auto comp = new LightComponent();
        comp->color = color;
        comp->name = name;

        Gfx::RegisterLight(comp);

        return comp;
    }

    LightComponent::~LightComponent() {
        Gfx::UnregisterLight(this);
    }

    void LightComponent::OnLoad() {
        Gfx::RegisterLight(this);
    }

    LightComponent::LightComponent()
    : name(""), color(1.0f) {}

    void LightComponent::SetColor(glm::vec3 new_color) {
        color = new_color;
    }

    glm::vec3 LightComponent::GetColor() const {
        return color;
    }
}*/
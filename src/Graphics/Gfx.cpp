
#include <glm/gtc/matrix_transform.hpp>
#include <glad/glad.h>
#include <sstream>
#include <GLFW/glfw3.h>
#include <TTH/Input/Input.h>
#include "TTH/Graphics/Gfx.h"
#include "Graphics_Out.h"

namespace TTH{
    PausableVector<RenderComponent*> Gfx::worldSpaceComponents;
    PausableSortedList<HUDComponent*, HUDComponent::zIndexComparator> Gfx::hudSpaceComponents;
    //PausableVector<HUDComponent*> Gfx::hudSpaceComponents;
    PausableVector<CameraComponent*> Gfx::cameras;
    PausableVector<LightComponent*> Gfx::lights;
    Object* Gfx::Canvas = nullptr;
    int Gfx::framebufferWidth;
    int Gfx::framebufferHeight;
    Texture* Gfx::lastFrame;
    bool Gfx::depthTestingEnabled = false;
    Framebuffer* Gfx::usedFramebuffer = nullptr;

    void Gfx::RegisterComponent(RenderComponent *rc) {
        worldSpaceComponents.push_back(rc);
        //std::cout << "Components - world: " << worldSpaceComponents.size() << " hud: " << hudSpaceComponents.size() << std::endl;
    }

    void Gfx::UnregisterComponent(RenderComponent *rc) {
        worldSpaceComponents.erase(std::remove_if(worldSpaceComponents.begin(), worldSpaceComponents.end(),[rc](auto r){return r == rc;}), worldSpaceComponents.end());
        //std::cout << "Components - world: " << worldSpaceComponents.size() << " hud: " << hudSpaceComponents.size() << std::endl;
    }

    void Gfx::Draw() {
        if(cameras.empty()){
            std::cerr << "Error - No camera registered" << std::endl;
            Input::QuitGame();
            return;
        }

        Clear();
        for(CameraComponent* c : cameras) {
            if(!c->IsActive()) continue;

            for(LightComponent* lc : lights){
                if(lc->CastsShadows()){
                    lc->CreateShadowMap();
                }
            }

            c->Use();
            Clear();
            if(c->drawWorldSpace)
                DrawWorldSpace(c);
            if(c->drawHUDSpace)
                DrawHUDSpace(c);
            c->EndUse();
        }

        CameraComponent* c = GetMainCamera();
        if(c) {
            c->Display();
            lastFrame = c->framebuffer->getTexture();
        }
    }

    void Gfx::RegisterLight(LightComponent *lc) {
        lights.push_back(lc);
    }

    void Gfx::UnregisterLight(LightComponent *lc) {
        lights.erase(std::remove_if(lights.begin(), lights.end(), [lc](auto l){return l==lc;}), lights.end());
    }

    void Gfx::RegisterComponent(HUDComponent *hc) {
        if(!hc->parent->HasParent()){
            if(!Canvas){
                CreateCanvas();
            }
            hc->parent->SetParent(Canvas);
        }
        //hudSpaceComponents.push_back(hc);
        hudSpaceComponents.Add(hc);
        //std::cout << "Components - world: " << worldSpaceComponents.size() << " hud: " << hudSpaceComponents.size() << std::endl;
    }

    void Gfx::UnregisterComponent(HUDComponent *hc) {
        //hudSpaceComponents.erase(std::remove_if(hudSpaceComponents.begin(), hudSpaceComponents.end(), [hc](auto c){return c==hc;}), hudSpaceComponents.end());
        hudSpaceComponents.Remove(hc);
        //if(hudSpaceComponents.empty()){
        //    RemoveCanvas();
        //}
        //std::cout << "Components - world: " << worldSpaceComponents.size() << " hud: " << hudSpaceComponents.size() << std::endl;
    }

    void Gfx::CreateCanvas() {
        if(Canvas != nullptr) RemoveCanvas();

        Canvas = new Object();
        Canvas->transform.position = glm::vec3(0.0f);
        Canvas->transform.rotation = glm::vec3(0.0f);
        Canvas->transform.scale = glm::vec3((float)framebufferWidth, (float)framebufferHeight,1.0f);
    }

    void Gfx::RemoveCanvas() {
        if(Canvas){
            delete Canvas;
            Canvas = nullptr;
        }
    }

    void Gfx::RegisterCamera(CameraComponent *cc) {
        cameras.push_back(cc);
    }

    void Gfx::UnregisterCamera(CameraComponent *cc) {
        cameras.erase(std::remove_if(cameras.begin(), cameras.end(), [cc](auto c){return cc == c;}), cameras.end());
    }

    void Gfx::Pause() {
        worldSpaceComponents.Pause();
        hudSpaceComponents.Pause();
        cameras.Pause();
        lights.Pause();
    }

    void Gfx::Resume() {
        worldSpaceComponents.Resume();
        hudSpaceComponents.Resume();
        cameras.Resume();
        lights.Resume();
    }

    void Gfx::ClearData() {
        worldSpaceComponents.ClearAll();
        hudSpaceComponents.ClearAll();
        cameras.ClearAll();
        lights.ClearAll();
    }

    void Gfx::GetResolution(int *width, int *height) {
        *width = framebufferWidth;
        *height = framebufferHeight;
    }

    Texture* Gfx::GetLastFrameTexture() {
        return lastFrame;
    }

    void Gfx::DisplayFramebuffer(const Framebuffer &framebuffer) {

        Shader* shader = ResourceManager::GetShader("framebuffer");
        shader->Use();
        shader->SetInt("screenTexture", 0);
        shader->SetInt("screenWidth", framebufferWidth);
        shader->SetInt("screenHeight", framebufferHeight);

        framebuffer.Display();
    }

    void Gfx::SetResolution(int width, int height) {
        framebufferWidth = width;
        framebufferHeight = height;
        if(Canvas != nullptr)
            CreateCanvas();
    }

    void Gfx::EnableDepthTest() {
        glEnable(GL_DEPTH_TEST);
        depthTestingEnabled = true;
    }

    void Gfx::DisableDepthTest() {
        glDisable(GL_DEPTH_TEST);
        depthTestingEnabled = false;
    }

    void Gfx::EnableBlending() {
        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    }

    void Gfx::DisableBlending() {
        glDisable(GL_BLEND);
    }

    void Gfx::Init() {
        glfwInit();
        glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
        glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
        glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
        glfwWindowHint(GLFW_RESIZABLE, GL_FALSE); // TODO: Config option
    }

    CameraComponent *Gfx::GetMainCamera() {
        for(CameraComponent* c : cameras){
            if(c->IsActive() && c->isMain)
                return c;
        }
        return nullptr;
    }

    const PausableVector<RenderComponent *> &Gfx::GetWorldSpaceComponents() {
        return worldSpaceComponents;
    }

    void Gfx::DrawWorldSpace(CameraComponent *c) {
        glm::mat4 projection = c->GetProjectionMatrix();
        glm::mat4 view = c->GetViewMatrix();
        glm::mat4 projectionView = projection * view;
        glm::vec3 viewPos = c->GetPosition();

        auto ubo = ResourceManager::GetUniformBuffer("Matrices");
        ubo->SetMat4(projection, 0);
        ubo->SetMat4(view, 64);
        ubo->SetMat4(projectionView, 128);

        for (RenderComponent* rc : worldSpaceComponents) {
            if(!rc->IsActive()) continue;

            rc->shader->Use();
            rc->shader->SetVec3("viewPos", viewPos);

            for (LightComponent* lc : lights) {
                if(!lc->IsActive()) continue;

                lc->SetUniforms(rc->shader);
            }

            rc->Draw();
        }
    }

    void Gfx::DrawHUDSpace(CameraComponent *c) {
        bool restoreDepthTesting = false;
        if(DepthTestEnabled()) {
            DisableDepthTest();
            restoreDepthTesting = true;
        }

        for (HUDComponent* hc : hudSpaceComponents) {
            if(!hc->IsActive()) continue;
            if(hc->shader) {
                hc->shader->Use();
                hc->shader->SetMat4("projection", c->GetHUDProjectionMatrix());
            }
            hc->Draw();
        }

        if(restoreDepthTesting) {
            EnableDepthTest();
        }
    }

    void Gfx::Clear() {
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    }

    bool Gfx::DepthTestEnabled() {
        return depthTestingEnabled;
    }
    void Gfx::UseFramebuffer(Framebuffer *fb) {
        usedFramebuffer = fb;
        usedFramebuffer->BindAsTarget();
    }

    void Gfx::UseNoFramebuffer() {
        if(usedFramebuffer) usedFramebuffer->Unbind();

        int w = Game::window.getWidth();
        int h = Game::window.getHeight();

        glViewport(0,0,w,h);

        usedFramebuffer = nullptr;
    }

    bool Gfx::IsUsingFramebuffer() {
        return usedFramebuffer != nullptr;
    }

    Framebuffer *Gfx::GetUsedFramebuffer() {
        return usedFramebuffer;
    }
}
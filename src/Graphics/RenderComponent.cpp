
#include <glm/gtc/matrix_transform.hpp>
#include <TTH/Graphics/RenderComponent.h>

#include "TTH/Graphics/RenderComponent.h"
#include "TTH/Graphics/Gfx.h"
#include "Graphics_Out.h"

namespace TTH {
    ComponentRegister<RenderComponent> RenderComponent::reg("RenderComponent");

    bool RenderComponent::LoadFromFile(IniFile &file) {
        file.SetToSection("RenderComponent");

        std::string modelName, shaderName;

        file.RequireValue("model", modelName);
        model = ResourceManager::GetModel(modelName);

        file.RequireValue("shader", shaderName);
        shader = ResourceManager::GetShader(shaderName);

        file.GetValue("colorTint", colorTint, glm::vec3(1.0f));

        file.GetValue("castsShadows", castsShadows, true);

        return true;
    }

    RenderComponent *RenderComponent::Clone() const {
        auto rc = new RenderComponent();
        rc->model = model;
        rc->shader = shader;
        rc->colorTint = colorTint;

        Gfx::RegisterComponent(rc);

        return rc;
    }

    RenderComponent::~RenderComponent() {
        Gfx::UnregisterComponent(this);
    }

    void RenderComponent::Draw() {
        shader->Use();
        shader->SetMat4("model", parent->GetWorldTransform().GetModelMatrix());
        shader->SetVec3("colorTint", colorTint);
        model->Draw(*shader);
    }

    void RenderComponent::OnLoad() {
        Gfx::RegisterComponent(this);
    }

    RenderComponent::RenderComponent()
            : model(nullptr), shader(nullptr), colorTint(1.0f) {}

    void RenderComponent::SetColor(glm::vec3 new_color) {
        colorTint = new_color;
    }

    void RenderComponent::SetModel(const std::string &newModelName) {
        model = ResourceManager::GetModel(newModelName);
    }

    void RenderComponent::SetShader(const std::string &newShaderName) {
        shader = ResourceManager::GetShader(newShaderName);
    }

    void RenderComponent::SetShader(Shader* newShader){
        shader = newShader;
    }

    const Model *RenderComponent::GetModel() const {
        return model;
    }

    Shader *RenderComponent::GetShader() const {
        return shader;
    }

    bool RenderComponent::CastsShadows() {
        return castsShadows;
    }
}
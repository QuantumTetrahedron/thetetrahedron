//
// Created by bartek on 22.12.2019.
//


#include <TTH/Graphics/ShadowMapper.h>
#include <glad/glad.h>
#include <TTH/Resources/ResourceManager.h>
#include <TTH/Graphics/RenderComponent.h>
#include <TTH/Graphics/Gfx.h>

namespace TTH{
    void ShadowMapper::FillShadowMap(TTH::ShadowMapper::ShadowMap &shadowMap, glm::mat4 lightSpaceMatrix) {
        shadowMap.Bind();
        glClear(GL_DEPTH_BUFFER_BIT);

        glEnable(GL_CULL_FACE);
        glCullFace(GL_FRONT);
        auto depthShader = ResourceManager::GetShader("depth");
        depthShader->Use();
        depthShader->SetMat4("lightSpaceMatrix", lightSpaceMatrix);
        shadowMap.SetLightSpaceMatrix(lightSpaceMatrix);

        //glEnable( GL_POLYGON_OFFSET_FILL );
        //glPolygonOffset( 2.0, 10000.0 );
        for (RenderComponent* rc : Gfx::GetWorldSpaceComponents()) {
            if(rc->IsActive() && rc->CastsShadows()) {
                depthShader->Use();
                depthShader->SetMat4("model", rc->GetParent()->GetWorldTransform().GetModelMatrix());
                TTH::Shader* s = rc->GetShader();
                rc->GetModel()->Draw(*depthShader);
                /*rc->SetShader(depthShader);
                rc->Draw();
                rc->SetShader(s);*/
            }
        }
        //glDisable( GL_POLYGON_OFFSET_FILL );
        glCullFace(GL_BACK);
        glDisable(GL_CULL_FACE);

        shadowMap.Unbind();
    }

    ShadowMapper::ShadowMap ShadowMapper::CreateNewShadowMap() {
        ShadowMap sm;
        sm.Generate();
        return sm;
    }

    void ShadowMapper::ShadowMap::Generate() {
        glGenFramebuffers(1, &depthMapFBO);

        const unsigned int width = 1024, height = 1024;

        glGenTextures(1, &depthMap);
        glBindTexture(GL_TEXTURE_2D, depthMap);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, width, height, 0, GL_DEPTH_COMPONENT, GL_FLOAT, 0);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
        float borderColor[] = { 1.0f, 1.0f, 1.0f, 1.0f };
        glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, borderColor);


        glBindFramebuffer(GL_FRAMEBUFFER, depthMapFBO);
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, depthMap, 0);
        glDrawBuffer(GL_NONE);
        glReadBuffer(GL_NONE);
        glBindFramebuffer(GL_FRAMEBUFFER,0);
    }

    void ShadowMapper::ShadowMap::UpdateShader(TTH::Shader *shader, std::string name, int texNum) {
        shader->SetMat4(name+".lightSpaceMatrix", lightSpaceMatrix);
        shader->SetInt(name+"_shadowMap", texNum);
        glActiveTexture(GL_TEXTURE0+texNum);
        glBindTexture(GL_TEXTURE_2D, depthMap);
    }

    void ShadowMapper::ShadowMap::Bind() const {
        glBindFramebuffer(GL_FRAMEBUFFER, depthMapFBO);
        glViewport(0,0,1024,1024);
    }

    void ShadowMapper::ShadowMap::Unbind() const {
        glBindFramebuffer(GL_FRAMEBUFFER, 0);
    }

    void ShadowMapper::ShadowMap::SetLightSpaceMatrix(glm::mat4 lsm) {
        lightSpaceMatrix = lsm;
    }
}

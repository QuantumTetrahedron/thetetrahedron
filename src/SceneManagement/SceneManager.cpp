
#include <algorithm>
#include <fstream>
#include "TTH/SceneManagement/SceneManager.h"
#include "SceneManagement_Out.h"

namespace TTH {
    SceneManager::Request SceneManager::request = SceneManager::Request::None;
    std::string SceneManager::sceneToLoad;

    void SceneManager::LoadNextScene(std::string sceneName) {
        ObjectManager::DestroyAllObjects();
        LoadObjects(sceneName);
    }

    void SceneManager::PauseAndLoadScene(std::string sceneName) {
        ObjectManager::Pause();
        Gfx::Pause();
        LoadObjects(sceneName);
    }

    void SceneManager::ResumeScene() {
        ObjectManager::DestroyObjects();
        ObjectManager::Resume();
        Gfx::Resume();
    }

    void SceneManager::Update(float dt) {
        ObjectManager::Update(dt);
    }

    void SceneManager::ProcessRequest(){
        if(request == Request::Load)
            LoadNextScene(sceneToLoad);
        else if(request == Request::PauseAndLoad)
            PauseAndLoadScene(sceneToLoad);
        else if(request == Request::Resume)
            ResumeScene();
        else if(request == Request::Clear)
            ClearAll();
        request = Request::None;
    }

    void SceneManager::SetStartingScene(std::string sceneName) {
        ObjectManager::DestroyAllObjects();
        LoadObjects(sceneName);
    }

    void SceneManager::ClearAll() {
        Gfx::ClearData();
        ObjectManager::DestroyAllObjects();
    }

    void SceneManager::LoadNextSceneRequest(std::string sceneName) {
        request = Request::Load;
        sceneToLoad = sceneName;
    }

    void SceneManager::PauseAndLoadSceneRequest(std::string sceneName) {
        request = Request::PauseAndLoad;
        sceneToLoad = sceneName;
    }

    void SceneManager::ResumeSceneRequest() {
        request = Request::Resume;
    }

    void SceneManager::ClearAllRequest() {
        request = Request::Clear;
    }

    void SceneManager::LoadObjects(const std::string &sceneName) {
        std::stack<std::string> parentingStack;
        std::string filePath = "Scenes/"+sceneName+"/"+sceneName+".lvl";
        std::ifstream file(filePath.c_str(), std::ios::in);

        std::string line;
        int lastLevel = 1;
        std::string lastName;

        while(getline(file, line)){
            if(line.size() == 0 || line[0] == ';') continue;

            std::stringstream line_ss(line);
            std::string levelStr, name, archetype;

            line_ss >> levelStr >> name >> archetype;
            int level = levelStr.size();

            if(level == lastLevel+1){
                parentingStack.push(lastName);
                lastLevel = level;
            } else if(level < lastLevel){
                while(level < lastLevel){
                    parentingStack.pop();
                    lastLevel--;
                }
            } else if(level > lastLevel+1){
                throw std::runtime_error("Wrong level hierarchy");
            }

            lastName = name;

            if(archetype.empty()){
                ObjectFactory::RemoveArchetype();
            } else {
                std::string archetypeFilePath = "Archetypes/" + archetype + ".ini";
                IniFile archetypeFile;
                archetypeFile.ReadFile(archetypeFilePath.c_str());
                ObjectFactory::SetArchetype(archetypeFile);
            }

            std::string objectFilePath = "Scenes/"+sceneName+"/"+name+".ini";
            IniFile objectFile;
            objectFile.ReadFile(objectFilePath.c_str());
            Object* o = ObjectFactory::Create(objectFile, name);
            if(o){
                ObjectManager::AddObject(o);
                if(!parentingStack.empty()){
                    o->SetParent(ObjectManager::GetObject(parentingStack.top()));
                }
            }
        }
        ObjectManager::Start();
    }

    void SceneManager::Init() {
        ObjectManager::Init();
    }

    bool SceneManager::IsSceneChanging() {
        return request != Request::None;
    }
}
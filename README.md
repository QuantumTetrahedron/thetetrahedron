# The Tetrahedron Engine

The Tetrahedron is a game engine written in C++ with the use of OpenGL.

Current engine version is far from being stable, but can already be used to create a very basic game.

## How to install

Install dependencies:
```
sudo apt-get install -y libglfw3-dev
sudo apt-get install -y libassimp-dev
sudo apt-get install -y libglm-dev
sudo apt-get install -y libfreetype6-dev
sudo cp -r /usr/include/freetype2/* /usr/include/
```

Compile from source by running:

```
cmake CMakeLists.txt
make
```

the .so file will be located inside the TTH directory.

Copy the libTheTetrahedron.so to your lib directory and copy the headers from TTH to some include/TTH directory.

```
sudo cp libTheTetrahedron.so /usr/local/lib/
sudo cp libTheTetrahedron.so.1 /usr/local/lib/
sudo cp libTheTetrahedron.so.1.0.0 /usr/local/lib/
sudo cp -r include/TTH /usr/local/include
```

## How to use

### Main

To run the engine, you only need 3 lines of code:

```
TTH::Game::Initialize("path/to/init/data");
TTH::Game::MainLoop();
TTH::Game::Shutdown();
```

### Config

The 'Initialize' method takes one argument - a path to a config file.

The file specifies all the necessary options and paths.

Consult the GameData/InitData.ini file for additional details.

### Objects

In order to create any object, a scene must be defined.

To do so, create a Scenes directory and put inside one directory for each scene you want to create.
The names of those directories are the scenes' names.

Inside every scene directory, a .ini file with the same name must be created. Inside this file should be
a list of every object that should be created when the scene loads. See the Scenes/TestScene/TestScene.ini for details.

To create an object, you need to create an .ini file in its scene directory. Consult Scenes/TestScene/TestObject1.ini for details.

### Engine Components

The engine provides the following components:

- Basics
    - BehaviourComponent
- For rendering
    - CameraComponent
    - RenderComponent
    - LightComponent
- For UI
    - SpriteComponent
    - TextComponent

### Custom components

If you wish to create a custom component, it should inherit from the TTH::BehaviourComponent abstract class.

Then it needs to override the following methods:

```
void Start()
void Update(float dt)
void OnLeave()
void LoadFromFile(IniFile& file)
YourComponentType* Clone() const
```

See the documentation of BehaviourComponent for details.

### Camera

To render the scene, at least one object on the scene must have a Camera Component attached with the mainCamera flag set to true.

### Input

Input reading must be defined in a .ini file.

See the documentation of Input for more details.